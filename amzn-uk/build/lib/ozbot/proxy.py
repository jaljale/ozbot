__author__ = 'krish'

import MySQLdb

def get_proxy(db, proxy):

    try:
        if not isinstance(db, MySQLdb.connection):
            db = MySQLdb.connect(host=db['host'],
                                 user=db['user'],
                                 passwd=db['passwd'],
                                 db=db['db'],
                                 charset=db['charset'],
                                 cursorclass=MySQLdb.cursors.DictCursor)


        sql = 'SELECT * FROM crawler_proxy WHERE id = "%s"' % proxy

        cursor = db.cursor()
        cursor.execute(sql)
        proxy = cursor.fetchone()
        cursor.close()
    except:
        proxy = 0

    return proxy
