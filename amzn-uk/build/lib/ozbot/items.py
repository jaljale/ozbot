

from scrapy.item import Item, Field

class OzbotItem(Item):
    ean = Field()
    asin = Field()
    title = Field()
    price = Field()
    weight = Field()
    shipping_weight = Field()
    dimension = Field()
    description = Field()
    image_url = Field()
    feature = Field()
    category_id = Field()
    last_updated = Field(serializer = str)
