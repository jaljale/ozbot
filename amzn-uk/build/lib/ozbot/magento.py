import MySQLdb
from time import gmtime, strftime
import MySQLdb.cursors
from ozbot import str

class Magento():

    def __init__(self):
        self.db = ''
        self.item = ''

    def _init_db(self):
        self.db = MySQLdb.connect(host="www.ozsom.com.au",
                         user="ozsom901_remote",
                          passwd="7x9qfLM&k7^M",
                          db="ozsom901_magen83", charset="utf8")

    def _get_insert_id(self, sku):
        cursor = self.db.cursor()
        cursor.execute("SELECT entity_id FROM sfy_catalog_product_entity WHERE sku = '%s'" % sku)
        rr = cursor.fetchone()

        if rr:
            return rr[0]
        else: return ''

    def update(self, item):
        self.item = item

        self._init_db()

        entity_id = self._get_insert_id(self.item['sku'])

        if not entity_id:

            sql = "INSERT into sfy_catalog_product_entity SET " \
						" entity_type_id=4, attribute_set_id=4, type_id='simple', sku='%s', " \
						" created_at='%s', updated_at='%s', " \
						" has_options=0, required_options=0" % (self.item['sku'], strftime("%Y-%m-%d %H:%M:%S", gmtime()), strftime("%Y-%m-%d %H:%M:%S", gmtime()))

            self.db.query(sql)
            self.db.commit()

            entity_id = self._get_insert_id(self.item['sku'])


        #self.db.autocommit(False)
        cursor = self.db.cursor()
        #try:
        cursor.execute("DELETE FROM sfy_catalog_product_entity_int WHERE entity_id=%s" % entity_id)
        cursor.execute("DELETE FROM sfy_catalog_product_entity_decimal WHERE entity_id=%s" % entity_id)
        cursor.execute("DELETE FROM sfy_catalog_product_entity_text WHERE entity_id=%s" % entity_id)
        cursor.execute("DELETE FROM sfy_catalog_product_entity_varchar WHERE entity_id=%s" % entity_id)
        cursor.execute("DELETE FROM sfy_catalog_product_entity_media_gallery WHERE entity_id=%s" % entity_id)
        cursor.execute("DELETE FROM sfy_catalog_category_product WHERE product_id=%s" % entity_id)
        cursor.execute("DELETE FROM sfy_catalog_product_website WHERE product_id=%s" % entity_id)
        cursor.execute("DELETE FROM sfy_cataloginventory_stock_item WHERE product_id=%s" % entity_id)

        self.db.commit()

        insert = "INSERT into sfy_catalog_product_entity_int (entity_type_id, attribute_id, store_id, `value`, entity_id) " \
                    "values (4, 96, 0, 1, %s)," \
                    "(4, 100, 0, 0, %s)," \
                    "(4, 121, 0, 2, %s)," \
                    "(4, 102, 0, 4, %s)" % (entity_id, entity_id, entity_id, entity_id)

        cursor.execute(insert)
        self.db.commit()

        #For price
        insert = "INSERT into sfy_catalog_product_entity_decimal (entity_type_id, attribute_id, store_id, `value`, entity_id) " \
            "values (4, 75, 0, '%s', %s)," \
            "(4, 80, 0, '%s', %s)" % (self.item['price'], entity_id, self.item['weight'], entity_id)

        cursor.execute(insert)


        #Description and short
        insert = "INSERT into sfy_catalog_product_entity_text (entity_type_id, attribute_id, store_id, value, entity_id)" \
                "values (4, 72, 0, '%s', %s)" % (self.db.escape_string(str.encode(self.item['description'])), entity_id)

        cursor.execute(insert)

        #Title etc
        insert = "INSERT into sfy_catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, value, entity_id) " \
                "values (4, 71, 0, '%s', %s)" % (self.db.escape_string(str.encode(self.item['name'])), entity_id)

        im = self.item['imagesall'].split(",")
        ct = 0
        for img in im:
            insert = "%s, (4, %s, 0, '%s', %s)" % (insert, 134+ct, self.db.escape_string(str.encode(img)), entity_id)
            ct = ct + 1

        #134	4	image1url
        #135	4	image2url
        #136	4	image3url
        #137	4	image4url
        #138	4	image5url
        #139	4	image6url

        print self.item
        insert = "%s, (4, 140, 0, '%s', %s)" % (insert, self.db.escape_string(str.encode(self.item['bullets'])), entity_id)
        insert = "%s, (4, 141, 0, '%s', %s)" % (insert, str.encode(self.item['length']), entity_id)
        insert = "%s, (4, 142, 0, '%s', %s)" % (insert, str.encode(self.item['width']), entity_id)
        insert = "%s, (4, 143, 0, '%s', %s)" % (insert, str.encode(self.item['height']), entity_id)
        insert = "%s, (4, 144, 0, '%s', %s)" % (insert, str.encode(self.item['shipping_weight']), entity_id)
        insert = "%s, (4, 145, 0, '%s', %s)" % (insert, self.db.escape_string(str.encode(self.item['imagesall'])), entity_id)

        print insert

        #141	4	length
        #142	4	width
        #143	4	height
        #144	4	shipping_weight
        #145	4	imagesall

        cursor.execute(insert)

        #Category association
        #if categoryId:
        #	cursor.execute("INSERT into catalog_category_product SET
        #	            category_id=" . cursor.quote(categoryId) . ",
        #	            product_id=" . cursor.quote(entity_id))

        #Website
        cursor.execute("INSERT into sfy_catalog_product_website SET " \
                            "product_id='%s', website_id=1" % entity_id)

        #Stock
        sql = "INSERT INTO sfy_cataloginventory_stock_item SET " \
                    "product_id= %s, " \
                    "stock_id=1, " \
                    "qty='0', " \
                    "min_qty='0', " \
                    "use_config_min_qty = '1', " \
                    "is_qty_decimal = '0', " \
                    "backorders = '0', " \
                    "use_config_backorders = '1', " \
                    "min_sale_qty = '0', " \
                    "use_config_min_sale_qty = '1', " \
                    "max_sale_qty = '0', " \
                    "use_config_max_sale_qty = '1', " \
                    "is_in_stock = '0', " \
                    "low_stock_date = '0000-00-00 00:00:00', " \
                    "notify_stock_qty = '0', " \
                    "use_config_notify_stock_qty = '1', " \
                    "manage_stock = '0', " \
                    "use_config_manage_stock = '1', " \
                    "stock_status_changed_auto = '1', " \
                    "use_config_qty_increments = '1', " \
                    "qty_increments = '0', " \
                    "use_config_enable_qty_inc = '1', " \
                    "enable_qty_increments = '0', " \
                    "is_decimal_divided = '0'" % entity_id

        cursor.execute(sql)
        cursor.close()
        self.db.commit()
        self.db.close()
        #except:
        #    self.db.rollback()
