__author__ = 'krish'



from ozbot.proxy import get_proxy

class ProxyMiddleware(object):

    proxy= ''
    db = ''

    def __init__(self, proxy, db):
        self.proxy = proxy
        self.db = db

    @classmethod
    def from_crawler(cls, crawler):

        print crawler.settings.get('PROXY_OBJ')

        return cls(crawler.settings.get('PROXY'), crawler.settings.get('DB_SETTING'))

    def process_request(self, request, spider):

        try:
            proxy = get_proxy(self.db, self.proxy)

            if proxy:
                address = 'http://%s' % proxy['proxy']
                request.meta['proxy'] = address
        except:
            pass