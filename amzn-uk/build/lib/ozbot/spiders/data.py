'''For data cleaning from html page'''
import re
import urllib
import json
from time import gmtime, strftime
from ozbot.items import OzbotItem
from ozbot import str
from scrapy.selector import Selector

class Data():
    '''cleaned and return data item'''

    def __init__(self, html, sel, asin, proxy):

        self.desc_stop_word  = ['see more']
        self.desc_replace_word = (['amazon.co.uk', 'ozsom.com.au'], ['Amazon', 'Ozsom'],)

        self.html = html
        self.sel = sel
        self.asin = asin
        self.item = OzbotItem()

        self.item['last_updated'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())

        if not proxy or not proxy['category_id']:
            self.item['category_id'] = 0
        else:
            self.item['category_id'] = proxy['category_id']

        #self._get_ean()

        self._get_title()
        self._get_price()
        self._get_description()
        self._get_images()
        self._get_weight_dimension()
        self._get_feature()
        self._get_shipping_weight()

        #cleaning
        self._clean_weight_dimension()   #see docstring comment why this.

        self._calculate_price()

    def _get_striped(self, string):
        if type(string) == list:
            string = string[0]

        return string.strip()

    def _get_float(self, string):
        '''remove the string and return floatingpoint'''
        float = re.search("(\d+)+\.?(\d+)*", string)
        return float.group(0).strip()

    def _get_title(self):
        '''get the title'''
        title = self.sel.xpath("//span[@id='productTitle']/text()").extract()
        if not title:
            title = self.sel.xpath("//span[@id='btAsinTitle']/span/text()").extract()

        try:
            title = self._get_striped(title)
        except:
            title = title[0]

        self.item['title'] = title

    def _get_price(self):
        '''get the price'''
        price = self.sel.xpath("//span[@id='priceblock_ourprice']/text()").extract()

        if not price:
            price = self.sel.xpath("//b[@class='priceLarge']/text()").extract()

        if price:
            try:
                price = self._get_float(self._get_striped(price))
            except:
                price = price

        if not price:
            price = ''

        self.item['price'] = price

    def _get_images(self, max = 6):
        '''Get the product image urls'''
        urls = []
        loop_count = 1
        try:
            image_js = re.search("colorImages(.*?)}\]}", self.html, re.MULTILINE).group(0)
            brace = re.search("\[", image_js)

            images = json.loads(image_js[brace.start():len(image_js)-1])

            for img in images:
                urls.append(self._get_striped(img['large']))
                if loop_count >= max: break
                loop_count = loop_count + 1

        except:
            pass

        self.item['image_url'] = urls

    def _clean_weight_dimension(self):
        '''
        1. Weight correction: Sometimes weight can be in dimension and shipping weight can be in boxed weight
        2. Measurement Unit: If weight in KG change it to Gram
        3. Break dimension into height, weight
        '''

        #get right weight
        if (self.item['dimension']):
            dm = self.item['dimension'].split(";")
            if len(dm) >= 2:
                self.item['dimension'] = dm[0]
                self.item['shipping_weight'] = self.item['weight']
                self.item['weight'] = dm[1]

            if not self.item['shipping_weight']:
                self.item['shipping_weight'] = self.item['weight']

        #convert weight into gram
        if self.item['shipping_weight']:
            self.item['shipping_weight'] = self._weight_unit_kg(self.item['shipping_weight'])

        if self.item['weight']:
            self.item['weight'] = self._weight_unit_kg(self.item['weight'])

        #format the dimension
        self._format_dimension()


    def _weight_unit_kg(self, weight):
        '''convert weight (if Kg) into gram'''
        new_weight = self._get_float(weight)
        wt = weight.strip().split(" ")
        if len(wt) >= 2:
            if wt[1].strip().lower() != 'kg':
                new_weight = float(wt[0].strip()) / 1000

        return new_weight

    def _calculate_price(self):

        if self.item['price'] and self.item['shipping_weight']:
            price = (float(self.item['price']) / 1.1) + (float(self.item['shipping_weight']) * 4.2)
            price = price * 1.86
            price = price * 1.45
            price = price - ((price- 30) / 9)
            price = price * 1.07
            price = round(price, 0)
            price = price - 0.01
            self.item['price'] = price

    def _format_dimension(self):
        '''format the dimension'''
        if self.item['dimension']:
            dm = self.item['dimension'].split('x')
            try:
                self.item['dimension'] = [self._get_float(dm[0]),
                                            self._get_float(dm[1]),
                                            self._get_float(dm[2])]
            except:
                pass


    def _get_weight_dimension(self):
        '''get product weight and dimension'''

        weight = dimension = ''

        try:
            wd = self.sel.xpath("//tr[@class='size-weight']/td")
            weight = wd[1].xpath("text()").extract()
            dimension = wd[3].xpath("text()").extract()
        except:
            wd = self.sel.xpath("//td[@class='bucket']/div[@class='content']/ul/li")

            re_patterns = ["Weight:(.*?)li>", "Dimensions:(.*?)li>"]
            for w in wd:
                match_weight = re.search(re_patterns[0], w.extract().replace("\n", "").replace("\t", ""))
                match_dimension = re.search(re_patterns[1], w.extract().replace("\n", "").replace("\t", ""))

                if match_weight:
                    weight = w.xpath("text()").extract()

                if match_dimension:
                    dimension = w.xpath("text()").extract()

        #domension can be in disclaim div
        if not dimension:
            dimension = self.sel.xpath("//div[@class='disclaim']/strong/text()").extract()

        try:
            weight = self._get_striped(weight)
        except:
            pass
        self.item['weight'] = weight

        try:
            dimension = self._get_striped(dimension)
        except:
            pass

        self.item['dimension'] = dimension


    def _get_description(self):
        '''Get the product description'''
        try:
            desc = re.search('var iframeContent =(.*?);', self.html, re.MULTILINE).group(0)
            #unquote and remove 'var iframeContent = '
            desc = urllib.unquote(desc).replace("var iframeContent = ", "")
            #desc = desc.replace("var iframeContent = ", "")
            #remove "
            desc = desc[1:len(desc)-2]
            #remove the all space and \n at begging and end
            desc = desc.strip("\n").strip(" ").strip("\n").strip(" ")

            sel = Selector(text=desc)
            body = sel.xpath("//body").extract()[0]
            desc = re.sub("(<div(.*?)Image\"(.*?)</div>)|(<h2>(.*?)</h2>)|(<div(.*?)emptyClear(.*?)</div>)|(\n)|(<table(.*?)</table>)|(<h(\d+)(.*?)>Product Description</h(\d+)>)|(<a(.*?)amazon.co.uk(.*?)</a>)|(class=\"seeAll\")", "", body)
            desc = re.sub("(<script>(.*?)</script>)", "", desc).strip()

        except:
            desc = ''

        if not desc:
            try:
                desc = self.sel.xpath("//div[@id='productDescription']").extract()
                #(<div(.*?)Image\"(.*?)</div>) = any image div
                # (<h2>(.*?)</h2>) = all h2
                # (<div(.*?)emptyClear(.*?)</div>) = emptyClr div
                # (\n) = line break
                # (<table(.*?)</table>) = table
                # (<h(\d+)(.*?)>Product Description</h(\d+)>) = product description
                # (<a(.*?)amazon.co.uk(.*?)</a>) = see more link, or any link goes to amazon
                # (class=\"seeAll\")" = see call class

                desc = re.sub("(<div(.*?)Image\"(.*?)</div>)|(<h2>(.*?)</h2>)|(<div(.*?)emptyClear(.*?)</div>)|(\n)|(<table(.*?)</table>)|(<h(\d+)(.*?)>Product Description</h(\d+)>)|(<a(.*?)amazon.co.uk(.*?)</a>)|(class=\"seeAll\")", "", desc[0])
            except:
                desc = ''

        if not desc: return ''

        desc = self._get_striped(desc)

        desc = desc.replace("\n", "").replace("\t", "")

        #filter words
        for w in self.desc_stop_word:
            desc = desc.replace(w, '')

        for w in self.desc_replace_word:
            desc = desc.replace(w[0], w[1])

        self.item['description'] = desc

    def _get_feature(self):
        feature = self.sel.xpath("//div[@id='feature-bullets']/span/ul").extract()

        if not feature:
            feature = self.sel.xpath("//div[@id='feature-bullets']/ul").extract()

        if not feature:
            feature = self.sel.xpath("//div[@id='technicalProductFeaturesATF']/ul").extract()

        try:
            feature = self._get_striped(feature)
        except:
            pass

        if not feature: feature = ''

        self.item['feature'] = feature.replace("\n", "").replace("\t", "")

    def _get_shipping_weight(self):
        weight = self.sel.xpath("//tr[@class='shipping-weight']/td[@class='value']/text()").extract()

        if not weight:
            try:
                weight = re.search("Boxed-product Weight:(.*?)li>", self.html, re.M).group(0)
                weight = weight.replace('Boxed-product Weight:', '')
                try:
                    weight = re.sub("(Boxed-product Weight:)|<.*?>", "", weight).strip()
                except:
                    pass
            except:
                weight = ''

        try:
            weight = self._get_striped(weight)
        except:
            pass

        self.item['shipping_weight'] = weight