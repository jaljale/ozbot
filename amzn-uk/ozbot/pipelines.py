import os
from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from scrapy.contrib.exporter import CsvItemExporter
import MySQLdb
import MySQLdb.cursors
from scrapy import log
from scrapy.exceptions import CloseSpider
from ozbot.proxy import get_proxy

class OzbotPipeline(object):

    def __init__(self):
        dispatcher.connect(self.spider_opened, signals.spider_opened)
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        dispatcher.connect(self.spider_error, signals.spider_error)

        self.files = {}

        # instace of MySQLdb
        self.db = ''

    def _connect_db(self, spider):

        if not self.db:
            db_setting = spider.crawler.settings.get('DB_SETTING')
            self.db = MySQLdb.connect(host = db_setting['host'],
                                      user = db_setting['user'],
                                      passwd = db_setting['passwd'],
                                      db = db_setting['db'],
                                      charset = db_setting['charset'],
                                      cursorclass=MySQLdb.cursors.DictCursor)

    def _proxy(self, spider):
        '''Update proxy uses settings and log'''
        proxy = spider.crawler.settings.get('PROXY')
        proxy_obj = get_proxy(self.db, proxy)

        if not proxy_obj:
            log.msg("No proxy found with id %s" % proxy, level=log.WARNING)
        else:
            log.msg("Using proxy %s (%s)" % (proxy, proxy_obj['proxy']), level=log.INFO)
            self.db.query("UPDATE crawler_proxy SET in_use = 1, stage3_status=0 WHERE id =%s" % proxy)
            self.db.commit()

    def _get_db_asin(self, asin):
        cursor = self.db.cursor()
        cursor.execute("SELECT asin FROM amazon WHERE asin = '%s'" % asin)
        rr = cursor.fetchone()

        if rr:
            return rr['asin']
        else: return ''

    def encode(self, string):
        return string.encode('UTF-8')

    def spider_error(self, response, spider):
        print "error ................................. on"
        print response.url

    def spider_opened(self, spider):
        self._connect_db(spider)
        self._proxy(spider)

        file = open('dump.csv', 'w+b')
        self.files[spider] = file
        self.exporter = CsvItemExporter(file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        #mark proxy not in use
        if not self.db:
            self._connect_db(spider)

        proxy = spider.crawler.settings.get('PROXY')
        self.db.query("UPDATE crawler_proxy SET in_use = 0, stage3_status=1 WHERE id = %s" % proxy)
        self.db.commit()

        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)

        if item['shipping_weight']:
            db_asin = self._get_db_asin(item['asin'].strip())

            if db_asin:
                insert = "UPDATE amazon SET "
                where = "WHERE asin = '%s'" % item['asin'].strip()
            else:
                insert = "INSERT INTO amazon SET "
                where = ''


            try:
                length = item['dimension'][0]
            except:
                length = 0
            try:
                height =  item['dimension'][1]
            except:
                height = 0
            try:
                width =  item['dimension'][2]
            except:
                width = 0

            title = self.db.escape_string(item['title'].encode("utf-8")).decode("utf-8")
            feature = self.db.escape_string(item['feature'].encode("utf-8")).decode("utf-8")
            description = self.db.escape_string(item['description'].encode("utf-8")).decode("utf-8")
            imagesall = ",".join(item['image_url'])
            imagesall = self.db.escape_string(imagesall)

            sql_set = " asin = '%s-%s'," \
                      "stock = '%s', " \
                      " `name` = '%s'," \
                      " price = '%s'," \
                      " shipping_weight = '%s'," \
                      " weight = '%s'," \
                      " bullets = '%s'," \
                      " imagesall = '%s', " \
                      " description = '%s', " \
                      " stage1_scrape_date = '%s', " \
                      " length = '%s', height = '%s', width = '%s', " \
                      " category_id = '%s', " \
                      " deliver_australia = '1', " \
                      " stage1_mage=1, stage2_mage = 1, stage3_mage = 1, " \
                      " stage2_amzn=1, stage3_amzn=1" %  (item['asin'].strip(), item['asin_add'].strip(),
                                                          item['stock'].strip(),
                                                          title,
                                                          item['price'],
                                                          item['shipping_weight'],
                                                          item['weight'],
                                                          feature,
                                                          imagesall,
                                                          description,
                                                          item['last_updated'],
                                                          length, height, width,
                                                          item['category_id'])


            sql = "%s %s %s" % (insert, sql_set, where)

            if not self.db:
                self._connect_db(spider)

            self.db.query(sql.encode("utf-8"))
            self.db.commit()

        else:
            log.msg(("Shipping weight not found for %s" % item['asin']), level=log.ERROR)

        return item