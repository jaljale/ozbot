from amazon.api import AmazonAPI

import MySQLdb
import MySQLdb.cursors

class Amazon():
    '''Return the item (amazon.api.AmazonProduct) based on ASIN'''

    access_key = "AKIAJD2JCCIBWQZFEO2Q"
    secret_key = "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"
    associate_tag = "ASSOCIATE TAG"

    amazon = AmazonAPI(access_key, secret_key, associate_tag, region='UK')

    def query(self, asin):
        return self.amazon.lookup(ItemId=asin,  ResponseGroup='BrowseNodes')


if __name__ == '__main__':
    db = MySQLdb.connect(host="127.0.0.1",
                                 user="root",
                                  passwd="",
                                  db="amazon", charset="utf8", cursorclass=MySQLdb.cursors.DictCursor)

    db2 = MySQLdb.connect(host="127.0.0.1",
                                 user="root",
                                  passwd="",
                                  db="amazon", charset="utf8", cursorclass=MySQLdb.cursors.DictCursor)
    cursor = db.cursor()
    cursor2 = db2.cursor()
    cursor.execute("SELECT a.id, a.asin, b.id FROM amazon a LEFT JOIN product_category b ON a.id = b.product_id where b.id is null")

    api = Amazon()

    while 1:
        item = cursor.fetchone()

        if not item: break

        asin = item['asin'].strip()

        print "Updating %s ... " % asin

        try:
            product = api.query(asin)
        except Exception as e:
            product = None
            print "Lookup failed: %s" % e.message.replace("\n", "").replace("\r", "")

        categories = {}
        category_id = []    #id from recently insert mysql table.
        if product:
            for category in product.browse_nodes:
                for parent in reversed(category.ancestors):
                    categories[parent.id]=parent.name
                categories[category.id] = category.name
                break

            parent_id = 0;
            for k, c in categories.iteritems():
                try:
                    c = str(c).encode("utf-8")
                except:
                    continue

                #check if category already exists.
                sql_select = 'SELECT * FROM category WHERE name="%s"' % \
                             db2.escape_string(c).decode("utf-8")
                cursor2.execute(sql_select.encode("utf-8"))
                data = cursor2.fetchone()
                if data:
                    parent_id = data['id']
                else:
                    sql = "INSERT into category SET amazon_id='%s', name='%s', parent_id='%s'" % \
                          (k, db2.escape_string(c).decode("utf-8"), parent_id)

                    cursor2.execute(sql.encode("utf-8"));
                    parent_id = cursor2.lastrowid
                category_id.append(parent_id)

            #product category assignment
            if category_id:
                sql2 = 'INSERT INTO product_category (category_id, product_id) VALUES'
                sqlval=''
                for cc in category_id:
                    sqlval = "%s(%s, %s)," % (sqlval, cc, item['id'])
                sqlval = sqlval[:-1]

                cursor2.execute('%s%s' % (sql2, sqlval));
                db2.commit()

    #cursor.close()