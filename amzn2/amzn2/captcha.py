
import os, sys, urllib
from amzn2 import str

path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(path)

captcha_path = path + "/../dump/"

try:
    from ozbot import deathbycaptcha
except:
    import deathbycaptcha


class Captcha():

    def __init__(self, sel, html, url):
        '''initialize url'''
        self.client = deathbycaptcha.SocketClient("teddy6565", "Jalpesh111")
        self.init(sel, html, url)


        #self._set_captcha_filename(filename)
        #self.captcha_file = 'captcha.jpg'

    def init(self, sel, html, url):
        ''' initiate again url '''
        self.sel = sel
        self.url = url
        self.html = html
        self.captcha_file = 'captcha.jpg'

        self._set_captcha_file()
        self._save_html()

        #self._set_captcha_filename(filename)
        #self.captcha_file = filename

    def _get_filename(self):
        ''' get the file name '''

        sp = self.url.split("/")
        return sp[len(sp)-1]

    def _set_captcha_file(self):
        '''sets the captcha filename'''

        self.captcha_file = self._get_filename() + ".jpg"

    def _save_html(self):
        '''save the html file'''

        html_file = self._get_filename() + ".html"
        open(captcha_path + html_file, 'wb').write(self.html)

    def _parse_captcha_url(self):
        '''get the captcha url'''
        url = self.sel.xpath("//div[@class='a-box-inner']/div/img/@src").extract()
        return str.get_striped(str.encode(url))

    def _parse_field_value(self):
        '''parse value in hidden input field'''
        dict = {}
        amzn = self.sel.xpath("//input[@name='amzn']/@value").extract()
        r = self.sel.xpath("//input[@name='amzn-r']/@value").extract()
        pt = self.sel.xpath("//input[@name='amzn-pt']/@value").extract()

        dict['amzn'] = str.get_striped(str.encode(amzn))
        dict['amzn-r'] = str.get_striped(str.encode(r))
        dict['amzn-pt'] = str.get_striped(str.encode(pt))

        return dict

    def _save_image(self, captcha_url):
        '''Download and save captcha URL'''
        captcha_img = urllib.urlopen(captcha_url).read()
        open(captcha_path + self.captcha_file, 'wb').write(captcha_img)

    def set_captcha_file(self, filename):
        '''set the captcha file name'''
        self.captcha_file = filename

    def get_string(self):
        '''get the captcha string from deathbycaptcha'''
        try:
            balance = self.client.get_balance()

            #save the captcha_image in local
            captcha_url = self._parse_captcha_url()
            self._save_image(captcha_url)

            captcha = self.client.decode(captcha_path + self.captcha_file, 20)
            if captcha:
                field_val = self._parse_field_value()
                captcha_str = str.get_striped(str.encode(captcha["text"]))
                field_val['field-keywords'] = captcha_str

                return field_val

        except deathbycaptcha.AccessDeniedException:
            pass

        return 0