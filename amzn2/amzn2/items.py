# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class Amzn2Item(Item):
    asin  = Field()
    delivery_message = Field()
    last_updated = Field()