import csv
import MySQLdb
import MySQLdb.cursors


class Db():
    db = MySQLdb.connect(host="127.0.0.1",
                         user="root",
                         passwd="",
                         db="amazon", charset="utf8", cursorclass=MySQLdb.cursors.DictCursor)

    def get_data(self):
        cursor = self.db.cursor()
        cursor.execute("select a.*, ic.*from amazon a, iain_category ic where a.asin = ic.sku")

        return cursor

if __name__ == '__main__':
    db = Db()
    cursor = db.get_data()

    file_handle = open("data.csv", "wb")
    csv_file = csv.writer(file_handle, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    csv_file.writerow(["Item Type", "Product ID", "Product Type", "Product Name",
                      "Product Code/SKU", "Brand Name", "Product Description", "Price",
                      "Product Weight", "Product Width", "Product Height", "Product Depth",
                      "Product Availability", "Track Inventory", "Current Stock Level", "Category",
                      "Product UPC/EA", "Product Image File - 1", "Product Image File - 2",
                      "Product Image File - 3", "Product Image File - 4", "Product Image File - 5"])

    while 1:
        item = cursor.fetchone()

        if not item:
            break

        #take five images, if images less number, just add blank
        images = item['imagesall'].split(",")[:5]
        for k in range(len(images), 5):
            images.append('')

        description = '<div class="bullets">%s</div><div class="description">%s</div>' % (item['bullets'].encode("utf8"), item['description'].encode("utf8"))

        data = ["product", item['bigid'], "P", item['name'].encode("utf8"),
                item['asin'], item['brand'].encode("utf8"), description, item['price'],
                item['shipping_weight'], item['width'], item['height'], item['length'],
                'Delivered within 3-5 working days', 'by product', item['stock'], item['category'].strip().encode("utf8"),
                item['ean']]
        data.extend(images)

        csv_file.writerow(data)
    file_handle.close()
