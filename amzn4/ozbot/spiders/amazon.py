import scrapy
import os
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy.exceptions import CloseSpider
from ozbot.spiders.data import Data
from ozbot.captcha import Captcha
from ozbot.proxy import get_proxy
from scrapy import log
from time import gmtime, strftime
import MySQLdb
import MySQLdb.cursors

class AmazonSpider(scrapy.Spider):
    name = "amazon"
    amazon_m = "http://www.amazon.co.uk/gp/product/"
    amazon_base = "http://www.amazon.co.uk/dp/"
    allowed_domains = ["amazon.co.uk"]
    start_urls = (
        'http://www.amazon.co.uk/',
    )

    captcha_count = 0

    proxy = 0

    def __init__(self, **kwargs):
        super(AmazonSpider, self).__init__(**kwargs)
        log_file = '%s.log' % strftime("log/%Y%m%d%H%M", gmtime())
        logfile = open(log_file, 'a')
        log_observer = log.ScrapyFileLogObserver(logfile, level=log.DEBUG)
        log_observer.start()

    def _get_source(self):
        db_setting = self.crawler.settings.get("DB_SETTING")
        db = MySQLdb.connect(host=db_setting['host'],
                             user=db_setting['user'],
                             passwd=db_setting['passwd'],
                             db=db_setting['db'],
                             charset=db_setting['charset'],
                             cursorclass=MySQLdb.cursors.DictCursor)


        sql = "SELECT asin FROM amazon2 WHERE name is NULL or name =''"

        cursor = db.cursor()
        cursor.execute(sql)

        return cursor

    def parse(self, response):

        cursor = self._get_source()
        referrer = response.url
        while 1:
            item = cursor.fetchone()
            if not item: break

            asin = item['asin'].strip()
            url = self.amazon_base + asin

            print "Crawling %s .... " % asin

            if referrer:
                rr = Request(url=url, callback=self.parse_item, headers={'Referer': referrer})
            else:
                rr = Request(url=url, callback=self.parse_item)

            rr.meta['asin'] = asin
            referrer = url
            yield(rr)

    def parse_item(self, response):
        '''Parse the data on response return, in case 200'''
        sel = Selector(response)

        if '404 - Document Not Found' in response.body:
            return None

        if 'Robot Check' in response.body:
            #if self.captcha_count >= 20:
            #    raise CloseSpider('captcha_pass_failed')

            captcha = Captcha(Selector(response), response.body, response.url)

            captcha_form = captcha.get_string()

            rr = FormRequest.from_response(response,
                    formdata=captcha_form,
                    callback=self.parse_item)
            rr.meta['asin'] = response.request.meta['asin']

            self.captcha_count = self.captcha_count + 1

            return rr

            #self._death_captcha(response)

        else:
            data = Data(response.body, sel, response.request.meta['asin'], self.proxy)
            data.item['asin'] = response.request.meta['asin']
            return data.item