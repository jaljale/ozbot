BOT_NAME = 'ozbot'

SPIDER_MODULES = ['ozbot.spiders']
NEWSPIDER_MODULE = 'ozbot.spiders'

ITEM_PIPELINES = ['ozbot.pipelines.OzbotPipeline']

#the user-agent
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.76.4 (KHTML, like Gecko) Version/7.0.4 Safari/537.76.4'

COOKIES_ENABLED = True
#COOKIES_DEBUG = True
AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_START_DELAY = 10
AUTOTHROTTLE_DEBUG = True

DOWNLOAD_DELAY = 1



DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
    'ozbot.middlewares.ProxyMiddleware': 100,
}

ITEM_PIPELINES = {
    'ozbot.pipelines.OzbotPipeline': 300,
}


PROXY = 0
DB_SETTING = {'host' : 'localhost',
              'user' : 'root',
              'passwd' : '',
              'db' : 'amazon',
              'charset' : 'utf8'}
PROXY_OBJ = ''