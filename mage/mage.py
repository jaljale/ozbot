'''
Magento migartion script
'''

import sys, os
sys.path.append(os.path.realpath(os.curdir + "/../"))

import MySQLdb
from time import gmtime, strftime
import MySQLdb.cursors
import ozutil

class Mage():

    def __init__(self):
        self.db = self.source_db = ''

        self._init_db()
        self._init_source_db()

    def _init_db(self):
        '''
        self.db = MySQLdb.connect(host="www.ozsom.com.au",
                         user="ozsom901_remote",
                          passwd="7x9qfLM&k7^M",
                          db="ozsom901_magen83", charset="utf8")
        '''

        self.db = MySQLdb.connect(host="localhost",
                         user="root",
                          passwd="",
                          db="magen83", charset="utf8")

    def _init_source_db(self):
        self.source_db = MySQLdb.connect(host="localhost",
                         user="root",
                          passwd="",
                          db="amazon", charset="utf8", cursorclass = MySQLdb.cursors.DictCursor)

    def _count_source(self, where):
        cursor = self.source_db.cursor()
        cursor.execute("%s WHERE %s" % ("SELECT count(asin) cnt FROM amazon", where))
        data = cursor.fetchone()

        cursor.close()

        return data['cnt']

    def _get_source(self, where):
        cursor = self.source_db.cursor()
        cursor.execute("SELECT * FROM amazon WHERE %s" % where)

        return cursor

    def _get_id(self, sku):
        cursor = self.db.cursor()
        cursor.execute("SELECT entity_id FROM sfy_catalog_product_entity WHERE sku = '%s'" % sku)
        rr = cursor.fetchone()
        cursor.close()

        if rr:
            return rr[0]
        else: return ''

    def _check_m2e(self, id):
        found = False

        cursor = self.db.cursor()
        sql = 'SELECT id FROM sfy_m2epro_listing_product WHERE product_id = "%s"' % id
        cursor.execute(sql)
        rr = cursor.fetchone()

        if rr:
            found = True
        else:
            sql = 'SELECT id FROM sfy_m2epro_listing_other WHERE product_id = "%s"' % id
            cursor.execute(sql)
            rr = cursor.fetchone()

            if rr:
                found = True

        cursor.close()

        return found

    def _get_stage1(self):
        return 'stage1_mage = 1'

    def _get_stage2(self):
        return 'stage2_mage = 1 and deliver_australia = 0'

    def _get_stage3(self):
        return 'stage3_mage = 1 and deliver_australia = 1'

    def _get_stage(self, stage):

        if stage == '1':
            return self._get_stage1()
        elif stage == '2':
            return self._get_stage2()
        elif stage == '3':
            return self._get_stage3()
        else:
            return 0

    def _export_stage1(self, data):
        '''
        Export the product to Magento. If product is already found, update it.
        '''
        if not data['ean']: raise Exception("No ean Found")

        entity_id = self._get_id(data['asin'])
        if not entity_id:
            sql = "INSERT into sfy_catalog_product_entity SET " \
						" entity_type_id=4, attribute_set_id=4, type_id='simple', sku='%s', " \
						" created_at='%s', updated_at='%s', " \
						" has_options=0, required_options=0" % (data['asin'], strftime("%Y-%m-%d %H:%M:%S", gmtime()), strftime("%Y-%m-%d %H:%M:%S", gmtime()))

            self.db.query(sql)
            self.db.commit()

            entity_id = self._get_id(data['asin'])


        self.db.autocommit(False)
        cursor = self.db.cursor()
        try:
            cursor.execute("DELETE FROM sfy_catalog_product_entity_int WHERE entity_id=%s" % entity_id)
            cursor.execute("DELETE FROM sfy_catalog_product_entity_decimal WHERE entity_id=%s" % entity_id)
            cursor.execute("DELETE FROM sfy_catalog_product_entity_text WHERE entity_id=%s" % entity_id)
            cursor.execute("DELETE FROM sfy_catalog_product_entity_varchar WHERE entity_id=%s" % entity_id)
            cursor.execute("DELETE FROM sfy_catalog_product_entity_media_gallery WHERE entity_id=%s" % entity_id)
            cursor.execute("DELETE FROM sfy_catalog_category_product WHERE product_id=%s" % entity_id)
            cursor.execute("DELETE FROM sfy_catalog_product_website WHERE product_id=%s" % entity_id)
            cursor.execute("DELETE FROM sfy_cataloginventory_stock_item WHERE product_id=%s" % entity_id)

            insert = "INSERT into sfy_catalog_product_entity_int (entity_type_id, attribute_id, store_id, `value`, entity_id) " \
                        "values (4, 96, 0, 1, %s)," \
                        "(4, 100, 0, 0, %s)," \
                        "(4, 121, 0, 2, %s)," \
                        "(4, 102, 0, 4, %s)" % (entity_id, entity_id, entity_id, entity_id)

            cursor.execute(insert)
            self.db.commit()

            #For price
            insert = "INSERT into sfy_catalog_product_entity_decimal (entity_type_id, attribute_id, store_id, `value`, entity_id) " \
                "values (4, 75, 0, '%s', %s)," \
                "(4, 80, 0, '%s', %s)" % (data['price'], entity_id, data['weight'], entity_id)

            cursor.execute(insert)


            #Description and short
            insert = "INSERT into sfy_catalog_product_entity_text (entity_type_id, attribute_id, store_id, value, entity_id)" \
                    "values (4, 72, 0, '%s', %s)" % (self.db.escape_string(ozutil.encode(data['description'])), entity_id)
            insert = "%s, (4, 147, 0, '%s', %s)" % (insert, self.db.escape_string(ozutil.encode(data['bullets'])), entity_id)
            insert = "%s, (4, 149, 0, '%s', %s)" % (insert, self.db.escape_string(ozutil.encode(data['imagesall'])), entity_id)

            cursor.execute(insert)

            #Title etc
            insert = "INSERT into sfy_catalog_product_entity_varchar (entity_type_id, attribute_id, store_id, value, entity_id) " \
                    "values (4, 71, 0, '%s', %s)" % (self.db.escape_string(ozutil.encode(data['name'])), entity_id)

            im = data['imagesall'].split(",")
            ct = 0
            for img in im:
                insert = "%s, (4, %s, 0, '%s', %s)" % (insert, 134+ct, self.db.escape_string(ozutil.encode(img)), entity_id)
                ct = ct + 1

            insert = "%s, (4, 141, 0, '%s', %s)" % (insert, ozutil.encode(data['length']), entity_id)
            insert = "%s, (4, 142, 0, '%s', %s)" % (insert, ozutil.encode(data['width']), entity_id)
            insert = "%s, (4, 143, 0, '%s', %s)" % (insert, ozutil.encode(data['height']), entity_id)
            insert = "%s, (4, 144, 0, '%s', %s)" % (insert, ozutil.encode(data['shipping_weight']), entity_id)
            insert = "%s, (4, 146, 0, '%s', %s)" % (insert, self.db.escape_string(data['ean']), entity_id)

            cursor.execute(insert)

            cursor.execute("INSERT into sfy_catalog_product_website SET " \
                                "product_id='%s', website_id=1" % entity_id)

            #Stock
            sql = "INSERT INTO sfy_cataloginventory_stock_item SET " \
                        "product_id= %s, " \
                        "stock_id=1, " \
                        "qty='10', " \
                        "min_qty='0', " \
                        "use_config_min_qty = '1', " \
                        "is_qty_decimal = '0', " \
                        "backorders = '0', " \
                        "use_config_backorders = '1', " \
                        "min_sale_qty = '0', " \
                        "use_config_min_sale_qty = '1', " \
                        "max_sale_qty = '0', " \
                        "use_config_max_sale_qty = '1', " \
                        "is_in_stock = '1', " \
                        "low_stock_date = '0000-00-00 00:00:00', " \
                        "notify_stock_qty = '0', " \
                        "use_config_notify_stock_qty = '1', " \
                        "manage_stock = '0', " \
                        "use_config_manage_stock = '1', " \
                        "stock_status_changed_auto = '1', " \
                        "use_config_qty_increments = '1', " \
                        "qty_increments = '0', " \
                        "use_config_enable_qty_inc = '1', " \
                        "enable_qty_increments = '0', " \
                        "is_decimal_divided = '0'" % entity_id

            cursor.execute(sql)

            if data['category_id']:
                #product category association
                sql = "DELETE FROM sfy_catalog_category_product where product_id = '%s' " % entity_id
                cursor.execute(sql)

                sql = "INSERT INTO sfy_catalog_category_product SET category_id='%s', product_id='%s'" % (data['category_id'], entity_id)
                cursor.execute(sql)
            else:
                print "No category given for product %s" % data['asin']

            cursor.close()
            self.db.commit()
            print "%s-%s done\n" % (data['asin'], entity_id)
            return True
        except Exception, e:
            print "Error: ", e
            self.db.rollback()

    def _export_stage2(self, data):
        m2e_update = False
        entity_id = self._get_id(data['asin'])
        if not entity_id:
            print "%s not found in Magento, please run stage1 first" % data['asin']
            print "%s not found in Magento, please run stage1 first" % data['asin']
            return 0
        else:
            #check m3epro_listing_product, m3epro_listing_other
            #m2e_update = self._check_m2e(entity_id)
            pass

        if data['deliver_australia'] != '1':
            cursor = self.db.cursor()
            try:
                self.db.autocommit(False)

                cursor.execute("DELETE FROM sfy_cataloginventory_stock_item WHERE product_id=%s" % entity_id)

                sql = "INSERT INTO sfy_cataloginventory_stock_item SET " \
                            "product_id= %s, " \
                            "stock_id=1, " \
                            "qty='0', " \
                            "min_qty='0', " \
                            "use_config_min_qty = '1', " \
                            "is_qty_decimal = '0', " \
                            "backorders = '0', " \
                            "use_config_backorders = '1', " \
                            "min_sale_qty = '0', " \
                            "use_config_min_sale_qty = '1', " \
                            "max_sale_qty = '0', " \
                            "use_config_max_sale_qty = '1', " \
                            "is_in_stock = '0', " \
                            "low_stock_date = '0000-00-00 00:00:00', " \
                            "notify_stock_qty = '0', " \
                            "use_config_notify_stock_qty = '1', " \
                            "manage_stock = '0', " \
                            "use_config_manage_stock = '1', " \
                            "stock_status_changed_auto = '1', " \
                            "use_config_qty_increments = '1', " \
                            "qty_increments = '0', " \
                            "use_config_enable_qty_inc = '1', " \
                            "enable_qty_increments = '0', " \
                            "is_decimal_divided = '0'" % (entity_id)

                cursor.execute(sql)

                '''
                if m2e_update:
                    sql_qty = "INSERT INTO `sfy_m2epro_product_change` (`product_id`, `store_id`, `action`, `attribute`, " \
                          " `value_old`, `value_new`, `creator_type`, `count_changes`, `update_date`, `create_date`) " \
                          " VALUES" \
                          " (%s, NULL, 'update', 'qty', '1', '0', 1, 1, NOW(), NOW())," \
                          " (%s, NULL, 'update', '__INSTANCE__', NULL, NULL, 1, NULL, NOW(), NOW()) " \
                          " (%s, NULL, 'update', 'stock_availability', '1', '0', 1, 1, NOW(), NOW()) " \
                          " " % (entity_id, entity_id, entity_id)
                    cursor.execute(sql_qty)
                '''
                cursor.close()
                self.db.commit()
                print "%s-%s stock set to 0\n" % (data['asin'], entity_id)
            except:
                self.db.rollback()

    def _export_stage3(self, data):
        m2e_update = False
        entity_id = self._get_id(data['asin'])
        if not entity_id:
            print "%s not found in Magento, please run stage1 first" % data['asin']
            return 0
        else:
            #check m3epro_listing_product, m3epro_listing_other
            m2e_update = self._check_m2e(entity_id)

        cursor = self.db.cursor()
        self.db.autocommit(False)
        try:
            cursor.execute("DELETE FROM sfy_catalog_product_entity_decimal WHERE entity_id=%s AND attribute_id=75" % entity_id)
            cursor.execute("DELETE FROM sfy_cataloginventory_stock_item WHERE product_id=%s" % entity_id)

            if not data['price'].strip():
                data['price'] = '0'
            else:
                data['price'] = data['price']

            insert = "INSERT into sfy_catalog_product_entity_decimal (entity_type_id, attribute_id, store_id, `value`, entity_id) " \
                "values (4, 75, 0, '%s', %s)" % (data['price'], entity_id)

            cursor.execute(insert)

            if not data['stock'].strip() or data['stock'] == '0':
                data['stock'] = '0'
                old_qty = '10'
                availability = '0'
            else:
                old_qty = '0'
                availability = '1'

            sql = "INSERT INTO sfy_cataloginventory_stock_item SET " \
                        "product_id= %s, " \
                        "stock_id=1, " \
                        "qty='%s', " \
                        "min_qty='0', " \
                        "use_config_min_qty = '1', " \
                        "is_qty_decimal = '0', " \
                        "backorders = '0', " \
                        "use_config_backorders = '1', " \
                        "min_sale_qty = '0', " \
                        "use_config_min_sale_qty = '1', " \
                        "max_sale_qty = '0', " \
                        "use_config_max_sale_qty = '1', " \
                        "is_in_stock = '%s', " \
                        "low_stock_date = '0000-00-00 00:00:00', " \
                        "notify_stock_qty = '0', " \
                        "use_config_notify_stock_qty = '1', " \
                        "manage_stock = '0', " \
                        "use_config_manage_stock = '1', " \
                        "stock_status_changed_auto = '1', " \
                        "use_config_qty_increments = '1', " \
                        "qty_increments = '0', " \
                        "use_config_enable_qty_inc = '1', " \
                        "enable_qty_increments = '0', " \
                        "is_decimal_divided = '0'" % (entity_id, data['stock'], availability)

            cursor.execute(sql)

            if m2e_update:

                sql_qty = "INSERT INTO `sfy_m2epro_product_change` (`product_id`, `store_id`, `action`, `attribute`, " \
                          " `value_old`, `value_new`, `creator_type`, `count_changes`, `update_date`, `create_date`) " \
                          " VALUES" \
                          " (%s, NULL, 'update', '__INSTANCE__', NULL, NULL, 1, NULL, NOW(), NOW()), " \
                          " (%s, NULL, 'update', 'qty', '%s', '%s', 1, 1, NOW(), NOW())" \
                          " " % (entity_id, entity_id, old_qty, data['stock'])
                if data['stock'] == '0':
                    sql_qty = "%s, (%s, NULL, 'update', 'stock_availability', '1', '0', 1, 1, NOW(), NOW())" % (sql_qty, entity_id)

                cursor.execute(sql_qty)

                sql_price  = "INSERT INTO `sfy_m2epro_product_change` (`product_id`, `store_id`, `action`, " \
                             " `attribute`, `value_old`, `value_new`, `creator_type`, `count_changes`, `update_date`, " \
                             " `create_date`) " \
                             " VALUES " \
                             " (%s, NULL, 'update', '__INSTANCE__', NULL, NULL, 1, NULL, NOW(), NOW()), " \
                             " (%s, NULL, 'update', 'price', '0.00', '%s', 1, 1, NOW(), NOW())" \
                             " " % (entity_id, entity_id, data['price'])

                cursor.execute(sql_price)

            cursor.close()
            self.db.commit()
            #print "%s-%s stock updated to %s\n" % (data['asin'], entity_id, data['stock'])
        except Exception, e:
            self.db.rollback()
            print e

    def _export(self, data, stage):
        if stage == '1':
            return self._export_stage1(data)
        elif stage == '2':
            return self._export_stage2(data)
        elif stage == '3':
            return self._export_stage3(data)
        else:
            return 0

    def _update_mage(self, data, stage):

        cursor = self.source_db.cursor()

        if stage == '1':
            sql = "stage1_mage = '0'"
        elif stage == '2':
            sql = "stage2_mage = '0'"
        elif stage == '3':
            sql = "stage3_mage = '0'"
            sql_update = "UPDATE crawler_mage SET run=1, run_dt='%s'" % strftime("%Y-%m-%d %H:%M:%S", gmtime())
            cursor.execute(sql_update)
        else:
            return

        sql = "UPDATE amazon SET %s WHERE asin = '%s'" % (sql, data['asin'])
        cursor.execute(sql)
        cursor.close()
        self.source_db.commit()

    def run(self, source, stage):
        where = self._get_stage(stage)
        count = self._count_source(where)
        source_cursor = self._get_source(where)

        print "%s%s (start): Migrating %s data for stage %s .........\n" % (source, stage, count, stage)

        while 1:
            data = source_cursor.fetchone()
            if not data: break;
            try:
                self._export(data, stage)
                self._update_mage(data, stage)
            except Exception, e:
                print "Export failed %s. Error: %s" % (data['asin'], str(e))

        source_cursor.close()


if __name__ == '__main__':

    syntax = "python mage.py mage|scrape stage\nWhere stage is either of 1, 2 or 3"
    if (len(sys.argv) < 3):
        raise Exception("Incorrect syntax.\n" + syntax)
    else:
        if sys.argv[2] != '1' and sys.argv[2] != '2' and sys.argv[2] != '3':
                raise Exception("Incorrect stage value.\n" + syntax)

        if sys.argv[1] != 'mage' and sys.argv[1] != 'scrape':
            raise Exception("The second param must be either mage or scrape\n" + syntax)


    mage = Mage()
    mage.run(sys.argv[1], sys.argv[2])