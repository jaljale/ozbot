'''
Magento migartion script
'''

import sys, os
from slugify import slugify
import MySQLdb
from time import gmtime, strftime
import MySQLdb.cursors
import traceback

sys.path.append(os.path.realpath(os.curdir + "/../"))

class Category():

    def __init__(self):
        self.db = self.source_db = ''

        self._init_db()
        self._init_source_db()

    def _init_db(self):
        '''
        self.db = MySQLdb.connect(host="www.ozsom.com.au",
                         user="ozsom901_remote",
                          passwd="7x9qfLM&k7^M",
                          db="ozsom901_magen83", charset="utf8")
        '''

        self.db = MySQLdb.connect(host="localhost",
                         user="root",
                          passwd="",
                          db="magen83", charset="utf8", cursorclass = MySQLdb.cursors.DictCursor)

    def _init_source_db(self):
        self.source_db = MySQLdb.connect(host="localhost",
                         user="root",
                          passwd="",
                          db="amazon", charset="utf8", cursorclass = MySQLdb.cursors.DictCursor)

    def _get_source(self):
        cursor = self.source_db.cursor()
        cursor.execute("SELECT * FROM iain_category WHERE category_id is NULL OR category_id = 0")

        return cursor

    def _get_category(self, name):
        sql = "SELECT * FROM sfy_catalog_category_entity_varchar where value='%s'" % self.db.escape_string(name)
        cursor = self.db.cursor()
        cursor.execute(sql)

        data = cursor.fetchone()

        if data:
            return data['entity_id']
        else:
            return None

    def _slugify(self, category_name):
        return slugify(category_name)

    def _export_category(self, category_name):

        category_id = self._get_category(category_name)
        if category_id: return category_id
        
        cursor = self.db.cursor()

        sql = "INSERT INTO `sfy_catalog_category_entity` (`entity_type_id`, `attribute_set_id`, `parent_id`, `position`, `path`, `level`, `children_count`) " \
              "VALUES " \
              "(3, 3, 2, 1, '1/2', 2, 0);";
        cursor.execute(sql)
        category_id = cursor.lastrowid

        sql = "UPDATE `sfy_catalog_category_entity` set `path` = '1/2/%s' WHERE entity_id = %s" % (category_id, category_id)
        cursor.execute(sql)

        sql = "INSERT INTO `sfy_catalog_category_entity_varchar`" \
              "(`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) " \
              "VALUES " \
              "(3, 41, 0, %s, '%s'), " \
              "(3, 46, 0, %s, NULL), " \
              "(3, 49, 0, %s, 'PRODUCTS'), " \
              "(3, 58, 0, %s, NULL), " \
              "(3, 61, 0, %s, NULL), " \
              "(3, 43, 0, %s, '%s') " % \
              (category_id, self.db.escape_string(category_name),
               category_id, category_id, category_id, category_id, category_id, self._slugify(category_name))
        cursor.execute(sql)

        sql = "INSERT INTO `sfy_catalog_category_entity_text` (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`)" \
              "VALUES " \
              "(3, 44, 0, %s, NULL), " \
              "(3, 47, 0, %s, NULL), " \
              "(3, 48, 0, %s, NULL), " \
              "(3, 62, 0, %s, NULL), " \
              "(3, 65, 0, %s, NULL); " % (category_id, category_id, category_id, category_id, category_id)
        cursor.execute(sql)

        sql = "INSERT INTO `sfy_catalog_category_entity_int` (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) " \
              "VALUES " \
              "(3, 42, 0, %s, 1), " \
              "(3, 67, 0, %s, 1), " \
              "(3, 50, 0, %s, NULL), " \
              "(3, 51, 0, %s, 0), " \
              "(3, 68, 0, %s, 0), " \
              "(3, 69, 0, %s, 0); " % (category_id, category_id, category_id, category_id, category_id, category_id)
        cursor.execute(sql)

        sql = "INSERT INTO `sfy_catalog_category_entity_decimal` (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) " \
              "VALUES " \
              "(3, 70, 0, %s, NULL); " % category_id
        cursor.execute(sql)

        sql = "INSERT INTO `sfy_catalog_category_entity_datetime` (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) " \
              "VALUES " \
              "(3, 59, 0, %s, NULL), " \
              "(3, 60, 0, %s, NULL);" % (category_id, category_id)
        cursor.execute(sql)
        self.db.commit();

        return category_id

    def _get_product(self, sku):
        sql = "SELECT * FROM sfy_catalog_product_entity WHERE sku='%s'" % sku
        cursor = self.db.cursor()
        cursor.execute(sql)
        return cursor.fetchone()

    def run(self):
        source = self._get_source()

        source_cursor = self.source_db.cursor()
        cursor = self.db.cursor()

        while 1:
            item = source.fetchone()
            if not item: break

            if not item['category']: continue

            print item

            try:
                category_id = self._export_category(item['category'])
            except:
                print "Error"
                traceback.print_exc()
                category_id = None

            if category_id:
                sql = "UPDATE iain_category SET category_id = %s WHERE id = %s" % (category_id, item['id'])
                source_cursor.execute(sql)
                self.source_db.commit()

                product = self._get_product(item['sku'])

                if product:
                    sql = "INSERT INTO `sfy_catalog_category_product` (`category_id`, `product_id`, `position`) " \
                          "VALUES (%s, %s, 0);" % (category_id, product['entity_id'])
                    cursor.execute(sql)

                    if not item['brand']: continue

                    brand = self.db.escape_string(item['brand'].encode("utf-8")).decode("utf-8")
                    sql = "INSERT INTO `sfy_catalog_product_entity_varchar` (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`)" \
                          "VALUES (4, 155, 0, %s, '%s');" % (product['entity_id'], brand)
                    cursor.execute(sql.encode("utf-8"))
                    self.db.commit()

if __name__ == '__main__':
    mage = Category()
    mage.run()