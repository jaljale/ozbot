'''
This files splits the products from amazon table for given category.
Proudct belongs to a category must provided in csv file.
'''

import csv
import MySQLdb
import MySQLdb.cursors


class Split():
    db = MySQLdb.connect(
        host="127.0.0.1",
        user="root",
        passwd="",
        db="amazon",
        charset="utf8",
        cursorclass=MySQLdb.cursors.DictCursor
    )

    def _update(self, asin, category):
        try:
            sql = "INSERT INTO iain_category set asin= '%s', category='%s'" % (
                asin,
                category
            )
            self.db.query(sql.encode("utf-8"))
            self.db.commit()
        except Exception as e:
            print "Error for %s %s" % (asin, category)
            print e

    def run(self, source_file):
        fh = open(source_file, "r")
        lines = csv.reader(fh, delimiter=",")
        for line in lines:
            if len(line) != 2:
                print "Error", line
            else:
                self._update(line[0], line[1])

if __name__ == '__main__':
    split = Split()
    split.run('iain_category.csv')
