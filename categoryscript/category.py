import os
import csv
from amazon.api import AmazonAPI


class Amazon():
    '''Return the item (amazon.api.AmazonProduct) based on ASIN'''

    access_key = "AKIAI6BTQ2ONUR35DENQ"
    secret_key = "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"
    associate_tag = "ASSOCIATE TAG"

    amazon = AmazonAPI(access_key, secret_key, associate_tag, region='UK')

    def query(self, asin):
        return self.amazon.lookup(ItemId=asin)


class CatTree(object):
    def __init__(self):
        self.source = "%s/%s" % (os.path.dirname(os.path.realpath(__file__)),
                                 'in.csv')
        self.out = "%s/%s" % (os.path.dirname(os.path.realpath(__file__)),
                              'out.csv')
        self.failed = "%s/%s" % (os.path.dirname(os.path.realpath(__file__)),
                              'failed.csv')

    def multi_category_to_string(self, categories):
        """Multiple category into string in format Cat/Cat1/..;Cat/Cat1/.."""
        cat_str = ''
        for cat in categories:
            cat_str1 = ''
            for tree in cat:
                cat_str1 = "%s/%s" % (cat_str1, tree)

            # skip the first by [1:]
            cat_str = "%s;%s" % (cat_str, cat_str1[1:])

        # skip the first ; by [1:]
        return cat_str[1:]

    def format(self, categories):
        """Fix the depth of category tree (4 level)"""
        formated_category = []
        for category in categories:
            length = len(category)

            diff = 4 - length

            if (diff < 0):
                category = category[:4]
            else:
                for i in range(diff):
                    category.append('')
            formated_category.append(category)

        return formated_category

    def run(self):
        api = Amazon()

        fh = open(self.source, "r")
        # open(self.out, "w").write("asin,Category1,Category2,Category3,Category4,CategoryBC,CategoryRaw\n")
        outfile = open(self.out, "wb")
        csvwriter = csv.writer(
            outfile,
            delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_MINIMAL
        )
        # write("asin,Category1,Category2,Category3,Category4,CategoryBC,CategoryRaw")

        while 1:
            asin = fh.readline()
            if not asin:
                break
            asin = asin.strip()

            try:
                product = api.query(asin)
            except Exception as e:
                product = None
                print "Lookup failed for %s: %s" % (asin, e.message.strip())
                open(self.failed, "a").write("%s\n" % asin)
                continue

            categories = []

            if product:
                i = 0
                for category in product.browse_nodes:
                    cats = []
                    i += 1

                    for parent in reversed(category.ancestors):
                        cats.append(parent.name)
                    cats.append(category.name)
                    # break

                    categories.append(cats)

                category_raw = self.multi_category_to_string(categories)
                formated_categories = self.format(categories)
                category_big_commerce = self.multi_category_to_string(
                    formated_categories
                )

            for cat in formated_categories:
                save_format = [asin]
                save_format.extend(cat)
                save_format.append(category_big_commerce)
                save_format.append(category_raw)

                csvwriter.writerow(save_format)
                # print save_format

                # open(self.out, "a").write(
                #     ",".join([col for col in save_format])
                # )

                # print save_format


if __name__ == '__main__':
    ct = CatTree()
    ct.run()
