# Scrapy settings for amzn2 project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'amzn2'

SPIDER_MODULES = ['amzn2.spiders']
NEWSPIDER_MODULE = 'amzn2.spiders'

ITEM_PIPELINES = ['amzn2.pipelines.Amzn2Pipeline']

#the user-agent
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.76.4 (KHTML, like Gecko) Version/7.0.4 Safari/537.76.4'

COOKIES_ENABLED = True
#COOKIES_DEBUG = True
AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_START_DELAY = 7

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
    'amzn2.middlewares.ProxyMiddleware': 100,
}

ITEM_PIPELINES = {
    'amzn2.pipelines.Amzn2Pipeline': 300,
}

PROXY = 0
DB_SETTING = {'host': 'localhost',
              'user': 'root',
              'passwd': '',
              'db': 'amazon',
              'charset': 'utf8'}

PROXY_OBJ = ''