from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from scrapy.contrib.exporter import CsvItemExporter
import MySQLdb
import MySQLdb.cursors
from proxy import get_proxy
from scrapy import log

class Amzn2Pipeline(object):
    def __init__(self):
        dispatcher.connect(self.spider_opened, signals.spider_opened)
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        self.files = {}

        self.db = ''

    def _connect_db(self, spider):
        db_setting = spider.crawler.settings.get('DB_SETTING')

        if not self.db:
            self.db = MySQLdb.connect(host=db_setting['host'],
                                      user=db_setting['user'],
                                      passwd=db_setting['passwd'],
                                      db=db_setting['db'],
                                      charset=db_setting['charset'],
                                      cursorclass=MySQLdb.cursors.DictCursor)

    def _proxy(self, spider):
        '''Update proxy uses settings and log'''

        proxy = spider.crawler.settings.get('PROXY')
        proxy_obj = get_proxy(self.db, proxy)
        if not proxy_obj:
            log.msg("No proxy found with id %s" % proxy, level=log.WARNING)
        else:
            log.msg("Using proxy %s (%s)" % (proxy, proxy_obj['proxy']), level=log.INFO)
            self.db.query("UPDATE crawler_proxy SET in_use = 1 WHERE id =%s" % proxy)
            self.db.commit()

    def spider_opened(self, spider):
        #proxycall
        self._connect_db(spider)
        self._proxy(spider)

        file = open('dump.csv', 'w+b')
        self.files[spider] = file
        self.exporter = CsvItemExporter(file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        #proxycall
        if not self.db:
            self._connect_db(spider)

        proxy = spider.crawler.settings.get('PROXY')
        self.db.query("UPDATE crawler_proxy SET in_use = 0 WHERE id = %s" % proxy)
        self.db.commit()

        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)

        if item['delivery_message']:
            deliver_australia = 1
        else:
            deliver_australia = 0

        if deliver_australia == 0:
            stock = ', stock = 0'
        else:
            stock = ''

        insert_sql = "UPDATE amazon2 SET " \
                        " deliver_australia = '%s'," \
                        " stage2_scrape_date = '%s', " \
                        " stage2_mage = 1 " \
                        " %s " \
                        " WHERE asin = '%s'" %  (deliver_australia, item['last_updated'], stock, item['asin'].strip())
        # connect db
        if not self.db:
            self._connect_db(spider)

        self.db.query(insert_sql)
        self.db.commit()

        return item
