import scrapy
import os
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy.exceptions import CloseSpider
from amzn2.spiders.data import Data
from amzn2.captcha import Captcha
import re
from amzn2.proxy import get_proxy
import MySQLdb
import MySQLdb.cursors
from scrapy import log
from time import gmtime, strftime

class AmazonSpider(scrapy.Spider):
    name = "amazon"
    amazon_m = "http://www.amazon.co.uk/gp/product/"
    amazon_base = "http://www.amazon.co.uk/dp/"

    allowed_domains = ["amazon.co.uk"]
    start_urls = (
        'https://www.amazon.co.uk',
    )

    headers = {'Content-Type': 'application/x-www-form-urlencoded', \
                   'X-Requested-With': 'XMLHttpRequest', \
                   'Connection': 'keep-alive', \
                   'Cache-Control': 'max-age=0', \
                   'X-Momentum-API-KEY': 'mBLAdWzDArSAcaAqgHsabA==', \
                   'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:29.0) Gecko/20100101 Firefox/29.0', \
                   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
    headers = {}

    captcha_count = 0
    test_count = 0

    def __init__(self):
        log_file = '%s.log' % strftime("log/%Y%m%d%H%M", gmtime())
        logfile = open(log_file, 'a')
        log_observer = log.ScrapyFileLogObserver(logfile, level=log.DEBUG)
        log_observer.start()

    def _get_source(self):
        '''get the source data'''
        db_setting = self.crawler.settings.get("DB_SETTING")
        db = MySQLdb.connect(host=db_setting['host'],
                             user=db_setting['user'],
                             passwd=db_setting['passwd'],
                             db=db_setting['db'],
                             charset=db_setting['charset'],
                             cursorclass=MySQLdb.cursors.DictCursor)

        proxy = self.crawler.settings.get("PROXY")
        proxy_obj = get_proxy(db, proxy)

        sql = "SELECT asin FROM amazon2 WHERE stage2_amzn = 1"

        if proxy_obj:
            range = ''
            if proxy_obj['stage2_from']:
                sql = '%s AND id >= %s' % (sql, proxy_obj['stage2_from'])
                range = '%s' % proxy_obj['stage2_from']
            else:
                range = 'MIN'
            if proxy_obj['stage2_to']:
                sql = '%s AND id <= %s' % (sql, proxy_obj['stage2_to'])
                range = '%s-%s' % (range, proxy_obj['stage2_to'])
            else:
                range = '%s-%s' % (range, 'MAX')
        else:
            range = 'MIN-MAX'

        log.msg('Scraping on range %s' % range, level=log.INFO)

        cursor = db.cursor()
        cursor.execute(sql)

        return cursor

    def parse(self, response):
        hxs = Selector(response)

        open('home.html', 'wb').write(response.body)

        if 'Iain' not in response.body:
            url = hxs.xpath("//a[@id='nav-link-yourAccount']/@href").extract()[0]
            print url
            rr = Request(url=url, callback=self.login)
            return rr

    def login(self, response):
        # if user is already logged in, do not login twice
        if not "Sign In" in response.body:
            url = "%s%s?smid=%s&test=3" % (self.amazon_base, 'B002APJCNE', "A3P5ROKL5A1OLE")
            rr = Request(url = url, callback = self.scrap_items, headers=self.headers)

            return rr

        self.headers['Referrer'] = response.url
        form_data = {'email': 'iain@ozsom.com.au', 'password': 'Jalpesh111'}
        rr = FormRequest.from_response(response,
                    formdata=form_data,
                    callback=self.test_deliver, headers=self.headers)

        return rr

    def test_deliver(self, response):
        open("login.html", "wb").write(response.body)
        sel = Selector(response)
        if "To continue, please answer" in response.body:
            form_data = {'dcq_question_subjective_1': '0422916767'}
            rr = FormRequest.from_response(response,
                    formdata=form_data,
                    callback=self.login, headers=self.headers)

            return rr

        self.headers['Referrer'] = response.url

        url = "%s%s?smid=%s" % (self.amazon_base, 'B002APJCNE', "A3P5ROKL5A1OLE")
        rr = Request(url = url, callback = self.test_result, headers=self.headers)
        return rr

    def test_result(self, response):
        open("B002APJCNE.html", "wb").write(response.body)

        self.headers['Referrer'] = response.url
        sel = Selector(response)

        try:
            if not re.search("Want it delivered to(.*?)Australia", response.body):
            # data = sel.xpath("//span[@id='deliveryMessage']").extract()[0]
            # data = re.sub("<(.*?>)|(\\xa0)", "", data)
            # if "Want it delivered to theAustralia" not in data:
                raise CloseSpider('"Want it delivered to the Australia" not found')
        except:
            raise CloseSpider('"Want it delivered to the Australia" not found')


        url = "%s%s?smid=%s&test=1" % (self.amazon_base, 'B002APJCNE', "A3P5ROKL5A1OLE")
        rr = Request(url = url, callback = self.scrap_items, headers=self.headers)

        return rr

    def scrap_items(self, response):
        self.headers['Referrer'] = response.url

        cursor = self._get_source()
        while 1:
            content = cursor.fetchone()
            if not content: break

            content = content['asin'].replace("\r", "").replace("\n", "")

            url = "%s%s?smid=%s" % (self.amazon_base, content, "A3P5ROKL5A1OLE")

            rr = Request(url=url, callback=self.parse_item, headers=self.headers)

            rr.meta['asin'] = content
            yield(rr)

    def parse_item(self, response):
        '''Parse the data on response return, in case 200'''
        sel = Selector(response)

        if 'Robot Check' in response.body:
            # if self.captcha_count >= 20:
            #    raise CloseSpider('captcha_pass_failed')

            captcha = Captcha(Selector(response), response.body, response.url)

            captcha_form = captcha.get_string()

            rr = FormRequest.from_response(
                response,
                formdata=captcha_form,
                callback=self.parse_item)
            rr.meta['asin'] = response.request.meta['asin']

            self.captcha_count = self.captcha_count + 1
            return rr

        else:
            data = Data(response.body, sel)
            data.item['asin'] = response.request.meta['asin']
            return data.item
