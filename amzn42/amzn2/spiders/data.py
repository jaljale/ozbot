'''For data cleaning from html page'''
import re
import urllib
import json
from time import gmtime, strftime
from amzn2.items import Amzn2Item
from amzn2 import str
from scrapy.selector import Selector
from BeautifulSoup import BeautifulSoup as Soup

class Data():
    '''cleaned and return data item'''
    def __init__(self, html, sel):

        self.html = html
        self.sel = sel
        self.item = Amzn2Item()

        self.item['last_updated'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        self._get_delivery_message()


    def encode(self, string):
        str_val = str.encode(string)
        return self._get_striped(str_val)

    def _get_striped(self, string):
        return str.get_striped(string)

    def _get_float(self, string):
        '''remove the string and return floatingpoint'''
        float = re.search("(\d+)+\.?(\d+)*", string)
        return float.group(0)

    def _get_delivery_message(self):
        try:
            self.item['delivery_message'] = re.search("Want it delivered to(.*?)Australia", self.html).group(0)
        except:
            self.item['delivery_message'] = ''

    def get_data(self):
        return self.item