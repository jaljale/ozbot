'''
This files splits the products from amazon table for given category.
Proudct belongs to a category must provided in csv file.
'''

import csv
import MySQLdb
import MySQLdb.cursors

class Split():

    db = MySQLdb.connect(host="127.0.0.1",
                                     user="root",
                                      passwd="",
                                      db="amazon", charset="utf8", cursorclass=MySQLdb.cursors.DictCursor)

    def _save_blank(self, asin):
        sql = "INSERT into amazon2 SET asin='%s'" % asin
        self.db.query(sql.encode("utf-8"))
        self.db.commit()

    def _copy_data(self, asin):
        sql = "insert into amazon2(asin, ean, name, imagesall,price, shipping_weight, weight, " \
              "bullets, description, length, width, height, stock, deliver_australia,stage1_scrape_date, " \
              "stage2_scrape_date, stage3_scrape_date, stage1_mage, stage2_mage,stage3_mage, stage2_amzn," \
              "stage3_amzn, category_id,stage3_amzn_error,category_tree)  select asin, ean, name, imagesall, " \
              "price, shipping_weight, weight, bullets, description, length, width, height, stock, deliver_australia, " \
              "stage1_scrape_date, stage2_scrape_date, stage3_scrape_date, stage1_mage, stage2_mage,stage3_mage, " \
              "stage2_amzn, stage3_amzn, category_id,stage3_amzn_error,category_tree from amazon where asin = '%s'" % asin
        self.db.query(sql.encode("utf-8"))
        self.db.commit()

    def migrate(self, asin):
        sql = "select * from amazon where asin='%s'" % asin.strip()
        cursor = self.db.cursor()
        cursor.execute(sql)

        item = cursor.fetchone()

        if not item:
            print 'blank'
            self._save_blank(asin)
        else:
            self._copy_data(asin)
        cursor.close()

    def run(self, source_file):
        fh = open(source_file, "r")

        while 1:
            content = fh.readline().strip()
            if not content: break
            self.migrate(content)

if __name__ == '__main__':
    split = Split()
    split.run('source.csv')