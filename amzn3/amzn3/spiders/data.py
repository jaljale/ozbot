'''For data cleaning from html page'''
import re
import urllib
import json
from time import gmtime, strftime
from amzn3.items import Amzn2Item
from amzn3 import str
from scrapy.selector import Selector
from BeautifulSoup import BeautifulSoup as Soup

class Data():
    '''cleaned and return data item'''
    def __init__(self, html, sel, item, date_check):

        self.html = html
        self.sel = sel
        self.item_data = item
        self.date_check = date_check

        self.item = Amzn2Item()
        self.item['asin'] = item['asin']

        self.item['last_updated'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        self._get_stock()
        self._get_price()

        self._calculate_price()

    def encode(self, string):
        str_val = str.encode(string)
        return self._get_striped(str_val)

    def _get_striped(self, string):
        return str.get_striped(string)

    def _get_float(self, string):
        '''remove the string and return floatingpoint'''
        float = re.search("(\d+)+\.?(\d+)*", string)
        return float.group(0)

    def _calculate_price(self):
        #try:

        if not self.item['price']:
            self.item['stock'] = 0
        else:
            if self.item['price'] and self.item_data['shipping_weight']:
                price = (float(self.item['price']) / 1.2) + (float(self.item_data['shipping_weight']) * 5.5)
                price = price * 1.95
                if (price > 40) :
                    price = price + 20
                else:
                    price = price +15
                price = round(price, 0)

                price = price - 0.01
                self.item['price'] = price

    def _get_price(self):
        price = 0
        pt1 = re.compile("Price(.*?)<br", re.I)
        price_txt = re.findall(pt1, self.html)

        if price_txt:
            price = re.sub("[^\d+\.]", "", price_txt[0])

        self.item['price'] = price

    def _get_stock(self):
        stock = 0
        self.item['raw_deliver'] = None

        pt = "Want it delivered by(.*?)\d+"
        data = re.search(pt, self.html)

        if data:
            #try:
            date = data.group(0)
            self.item['raw_deliver'] = date
            if self.date_check  in date:
                stock = 10

            #except:
            #    stock = 0
        else:
            stock = 0

        if stock:
            pt2 = re.compile("Only (\d+) left in stock", re.I)
            found2 = re.findall(pt2, self.html)

            if found2:
                stock = found2[0]

        self.item['stock'] = stock



    def get_data(self):
        return self.item