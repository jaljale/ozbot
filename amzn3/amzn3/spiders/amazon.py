import scrapy
import re
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy.exceptions import CloseSpider
from amzn3.spiders.data import Data
from amzn3.captcha import Captcha
from amzn3.proxy import get_proxy
import MySQLdb
import MySQLdb.cursors
from scrapy import log
from time import gmtime, strftime

class AmazonSpider(scrapy.Spider):
    name = "amazon"
    amazon_m = "http://www.amazon.co.uk/gp/aw/d/"
    amazon_base = "http://www.amazon.co.uk/dp/"

    date_check = ''

    allowed_domains = ["amazon.co.uk"]
    start_urls = (
        'http://m.amazon.co.uk',
    )

    captcha_count = 0

    headers = {'Content-Type': 'application/x-www-form-urlencoded',
               'X-Requested-With': 'XMLHttpRequest',
               'Connection': 'keep-alive',
               'Cache-Control': 'max-age=0',
               'X-Momentum-API-KEY': 'mBLAdWzDArSAcaAqgHsabA==',
               'User-Agent':'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}
    headers = {} #no header

    def __init__(self):
        log_file = '%s.log' % strftime("log/%Y%m%d%H%M", gmtime())
        logfile = open(log_file, 'a')
        log_observer = log.ScrapyFileLogObserver(logfile, level=log.DEBUG)
        log_observer.start()

    def _get_source(self):
        db_setting = self.crawler.settings.get("DB_SETTING")
        db = MySQLdb.connect(host=db_setting['host'],
                             user=db_setting['user'],
                             passwd=db_setting['passwd'],
                             db=db_setting['db'],
                             charset=db_setting['charset'],
                             cursorclass=MySQLdb.cursors.DictCursor)

        proxy = self.crawler.settings.get("PROXY")
        proxy_obj = get_proxy(db, proxy)

        sql = "SELECT asin, shipping_weight, price FROM amazon WHERE stage3_amzn = 1 AND deliver_australia = 1"

        if proxy_obj:
            range = ''
            if proxy_obj['stage3_from']:
                sql = '%s AND id >= %s' % (sql, proxy_obj['stage3_from'])
                range = '%s' % proxy_obj['stage3_from']
            else:
                range = 'MIN'
            if proxy_obj['stage3_to']:
                sql = '%s AND id <= %s' % (sql, proxy_obj['stage3_to'])
                range = '%s-%s' % (range, proxy_obj['stage3_to'])
            else:
                range = '%s-%s' % (range, 'MAX')
        else:
            range = 'MIN-MAX'

        if self.crawler.settings.get('ERROR_ONLY'):
            sql = '%s AND stage3_amzn_error = 1' % sql
            range = '%s, error only' % range

        log.msg('Scraping on range %s' % range, level=log.INFO)

        cursor = db.cursor()
        cursor.execute(sql)

        return cursor

    def parse(self, response):
        hxs = Selector(response)

        url = hxs.xpath("//a[contains(text(), 'Sign Out')]/@href").extract()
        if not url:
            url = hxs.xpath("//a[contains(text(), 'Sign In')]/@href").extract()

        if len(url) >= 1:
            url = url[0]

        rr  = Request(url=url, callback=self.login)
        return rr

    def login(self, response):
        open('login_page.html', 'wb').write(response.body)

        form_data = {'email': 'iain@ozsom.com.au', 'password': 'Jalpesh111'}
        rr = FormRequest.from_response(response,
                    formdata=form_data,
                    callback=self.test_deliver, dont_click=True)

        return rr

    def test_deliver(self, response):
        open('login.html', 'wb').write(response.body)

        if "To continue, please answer" in response.body:
            form_data = {'dcq_question_subjective_1': '0422916766'}
            rr = FormRequest.from_response(response,
                        formdata=form_data,
                        callback=self.q_answer, dont_click=True)
            return rr
        elif 'Iain!' not in response.body:
            raise CloseSpider('Can not login to amazon.co.uk!')
        else:
            url = "%s%s?smid=%s" % (self.amazon_m, 'B002APJCNE', "A3P5ROKL5A1OLE")
            rr = Request(url = url, callback = self.test_result)
            return rr

    def q_answer(self, response):
        open('answer.html', 'wb').write(response.body)

        if 'Iain!' in response.body:
            url = "%s%s?smid=%s" % (self.amazon_m, 'B002APJCNE', "A3P5ROKL5A1OLE")
            rr = Request(url = url, callback = self.test_result)
            return rr
        else:
            raise CloseSpider('Can not login to amazon.co.uk!')

    def test_result(self, response):
        open("B002APJCNE.html", "wb").write(response.body)

        pt = "Want it delivered by(.*?)\d+"
        data = re.search(pt, response.body)
        try:
            date = re.sub("<.*?>", "", data.group(0)).split(",")[1].strip()
        except:
            date = None

        if not date:
            raise CloseSpider('Delivery date for sample product B002APJCNE not found')

        self.date_check = date

        url = "%s%s?smid=%s&test=1" % (self.amazon_m, 'B002APJCNE', "A3P5ROKL5A1OLE")
        rr = Request(url = url, callback = self.scrap_items)

        return rr

    def scrap_items(self, response):
        open('ref_item.html', 'wb').write(response.body)

        referrer = response.url

        cursor = self._get_source()
        while 1:
            item = cursor.fetchone()
            if not item: break

            content = item['asin'].replace("\r", "").replace("\n", "")

            url = "%s%s/ref=sr_1_3?m=%s" % (self.amazon_m, content, "A3P5ROKL5A1OLE")

            if referrer:
                self.headers['Referrer']  = referrer
                rr = Request(url=url, callback=self.parse_item, headers=self.headers)
            else:
                rr = Request(url=url, callback=self.parse_item)

            rr.meta['item'] = item
            referrer = url
            yield(rr)

    def parse_item(self, response):
        '''Parse the data on response return, in case 200'''
        sel = Selector(response)

        if 'Robot Check' in response.body:
            #if self.captcha_count >= 20:
            #    raise CloseSpider('captcha_pass_failed')

            captcha = Captcha(Selector(response), response.body, response.url)

            captcha_form = captcha.get_string()

            rr = FormRequest.from_response(response,
                    formdata=captcha_form,
                    callback=self.parse_item)
            rr.meta['item'] = response.request.meta['item']

            self.captcha_count = self.captcha_count + 1

            return rr
        else:
            try:
                data = Data(response.body, sel, response.request.meta['item'], self.date_check)
                return data.item
            except:
                log.msg("Error with item " + response.request.meta['item']['asin'], level=log.ERROR)
                return None