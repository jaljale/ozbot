__author__ = 'krish'
__email__ = 'krish@jaljale.com'

from amzn3.proxy import get_proxy

class ProxyMiddleware(object):

    def __init__(self, proxy, db):
        self.proxy = get_proxy(db, proxy)

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.get('PROXY'), crawler.settings.get('DB_SETTING'))

    def process_request(self, request, spider):
        if self.proxy:
            address = 'http://%s' % self.proxy['proxy']
            request.meta['proxy'] = address