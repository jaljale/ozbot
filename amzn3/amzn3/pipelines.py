from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
import sys
import os
from time import gmtime, strftime
import MySQLdb
import MySQLdb.cursors
from proxy import get_proxy
from scrapy import log

class Amzn2Pipeline(object):

    def __init__(self):
        self.db = ''
        dispatcher.connect(self.spider_opened, signals.spider_opened)
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        dispatcher.connect(self.spider_error, signals.spider_error)

    def _connect_db(self, spider):
        if not self.db:
            db_setting = spider.crawler.settings.get('DB_SETTING')
            self.db = MySQLdb.connect(host=db_setting['host'],
                                      user=db_setting['user'],
                                      passwd=db_setting['passwd'],
                                      db=db_setting['db'],
                                      charset=db_setting['charset'],
                                      cursorclass=MySQLdb.cursors.DictCursor)
    def _proxy(self, spider):
        '''Update proxy uses settings and log'''
        proxy = spider.crawler.settings.get('PROXY')
        proxy_obj = get_proxy(self.db, proxy)

        if not proxy_obj:
            log.msg("No proxy found with id %s" % proxy, level=log.WARNING)
        else:
            log.msg("Using proxy %s (%s)" % (proxy, proxy_obj['proxy']), level=log.INFO)
            date = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            self.db.query("UPDATE crawler_proxy SET in_use = 1, stage3_status=0, in_use_from='%s' WHERE id =%s" % (date, proxy))
            self.db.commit()

    def _get_data(self, item):
        sql = "SELECT price, stock FROM amazon WHERE asin = '%s'" % item['asin']
        cursor = self.db.cursor()
        cursor.execute(sql)
        return cursor.fetchone()

    def _can_update(self, source, item):
        if not source:
            return True

        #if stock is 0, just update
        if float(item['stock']) < 1 or not float(source['price']):
            return True
        
        #update only if price change is greather than 7.5%
        if abs(float(source['price']) - float(item['price'])) / float(source['price']) > 0.075:
            return True

        #update only if stock change is greater than 2 item
        if abs(float(source['stock']) - float(item['stock'])) > 2:
            return True

    def spider_opened(self, spider):
        self._connect_db(spider)
        self._proxy(spider)

    def spider_closed(self, spider):
        #mark proxy not in use
        if not self.db:
            self._connect_db(spider)

        proxy = spider.crawler.settings.get('PROXY')
        self.db.query("UPDATE crawler_proxy SET in_use = 0, stage3_status=1 WHERE id = %s" % proxy)
        self.db.commit()

        #can run mage3
        sql = "SELECT count(id) cnt FROM crawler_proxy WHERE stage3_status=0"
        cursor = self.db.cursor()
        cursor.execute(sql)
        item = cursor.fetchone()

        if item['cnt'] < 1:
            #update status
            date = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            sql = "UPDATE crawler_crawler_mage SET run_dt = '%s'" % date
            self.db.commit()

            #run mage3
            path = os.path.realpath(os.curdir) + "/../mage/"
            sys.path.append(path)
            from mage import Mage
            m = Mage()
            m.run('mage', '3')

    def spider_error(self, response, spider):
        #mark proxy not in use
        if not self.db:
            self._connect_db(spider)

        item = response.meta['item']

        sql = "UPDATE amazon SET stage3_amzn_error = '1' WHERE asin='%s'" % (item['asin'].strip())
        self.db.query(sql)
        self.db.commit()

    def process_item(self, item, spider):
        if not self.db:
            self._connect_db(spider)

        data = self._get_data(item)
        can_import = self._can_update(data, item)

        #if can_import:
        #in case item has price 0, set set stock 0
        if float(item['price']) < 1:
            item['stock'] = 0

        insert_sql = " price = '%s', stock = '%s', stage3_mage = 1, stage3_scrape_date = '%s' " % \
                     (item['price'], item['stock'], item['last_updated'])

        if spider.crawler.settings.get('ERROR_ONLY'):
            insert_sql = '%s, stage3_amzn_error=0 ' % insert_sql

        insert_sql = "UPDATE amazon SET %s WHERE asin = '%s'" % (insert_sql, item['asin'].strip())

        self.db.query(insert_sql)
        self.db.commit()

        return item
