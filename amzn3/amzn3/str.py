
def encode(string):
    if not string: return ''

    if type(string) == list:
        str = string[0].encode("UTF-8")
    else:
        str = string.encode("UTF-8")

    return str

def get_striped(string):
    return string.strip("\n").strip(" ").strip("\n").strip(" ")