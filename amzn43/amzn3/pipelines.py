from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
import sys
import os
from time import gmtime, strftime
import MySQLdb
import MySQLdb.cursors
from proxy import get_proxy
from scrapy import log


class Amzn2Pipeline(object):

    def __init__(self):
        self.db = ''
        self.ebaycsv = ''
        self.bigcsv = ''
        self.magecsv = ''
        dispatcher.connect(self.spider_opened, signals.spider_opened)
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        dispatcher.connect(self.spider_error, signals.spider_error)

    def _connect_db(self, spider):
        if not self.db:
            db_setting = spider.crawler.settings.get('DB_SETTING')
            self.db = MySQLdb.connect(host=db_setting['host'],
                                      user=db_setting['user'],
                                      passwd=db_setting['passwd'],
                                      db=db_setting['db'],
                                      charset=db_setting['charset'],
                                      cursorclass=MySQLdb.cursors.DictCursor)

    def _proxy(self, spider):
        '''Update proxy uses settings and log'''
        proxy = spider.crawler.settings.get('PROXY')
        proxy_obj = get_proxy(self.db, proxy)

        if not proxy_obj:
            log.msg("No proxy found with id %s" % proxy, level=log.WARNING)
        else:
            log.msg("Using proxy %s (%s)" % (proxy, proxy_obj['proxy']), level=log.INFO)
            date = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            self.db.query("UPDATE crawler_proxy SET in_use = 1, stage3_status=0, in_use_from='%s' WHERE id =%s" % (date, proxy))
            self.db.commit()

    def _get_data(self, item):
        sql = "SELECT price, stock FROM amazon2 WHERE asin = '%s'" % item['asin']
        cursor = self.db.cursor()
        cursor.execute(sql)
        return cursor.fetchone()

    def _get_category(self, item):
        sql = "SELECT * FROM iain_category WHERE asin = '%s'" % item['asin']
        cursor = self.db.cursor()
        cursor.execute(sql)
        data = cursor.fetchone()

        if data:
            return data['category']
        else:
            return ''

    def _can_update(self, source, item):
        if not source:
            return True

        # if stock is 0, just update
        if float(item['stock']) < 1 or not float(source['price']):
            return True
        # update only if price change is greather than 7.5%
        if abs(float(source['price']) - float(item['price'])) / float(source['price']) > 0.075:
            return True

        # update only if stock change is greater than 2 item
        if abs(float(source['stock']) - float(item['stock'])) > 2:
            return True

    def spider_opened(self, spider):
        self._connect_db(spider)
        self._proxy(spider)

    def spider_closed(self, spider):
        # mark proxy not in use
        if not self.db:
            self._connect_db(spider)

        proxy = spider.crawler.settings.get('PROXY')
        export_path = spider.crawler.settings.get('EXPORT_PATH')
        export_debug = spider.crawler.settings.get('EXPORT_DEBUG')

        date = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        self.db.query("UPDATE crawler_proxy SET in_use = 0, ended_at='%s', stage3_status=1 WHERE id = %s" % (date, proxy))
        self.db.commit()

        # export mage, big and ebay csv

        print "csv making started"
        sql = "SELECT * FROM amazon2"

        if export_debug:
            sql = '%s WHERE stage3_amzn = 1 limit %s' % (sql, export_debug)

        # if not os.path.isfile(self.ebaycsv):
        self.ebaycsv = '%sebay.csv' % export_path
        self.bigcsv = '%sbig.csv' % export_path
        self.magecsv = '%smage.csv' % export_path

        open(self.ebaycsv, "w").write("Action,ItemID,StartPrice,Quantity\n")
        open(self.bigcsv, "w").write("Product ID, Product Name, Category, Price, Weight, Current Stock Level\n")
        open(self.magecsv, "w").write("sku,qty,special_price,price,is_in_stock\n")

        cursor = self.db.cursor()
        cursor.execute(sql)
        while 1:
            item = cursor.fetchone()
            if not item: break

            # Write in file
            try:
                stock = int(item['stock'])
                if stock > 1 and item['shipping_weight']:
                    ebay_stock = 1
                else:
                    ebay_stock = 0
            except:
                ebay_stock = 0

            line = "Revise,%s,%s,%s\n" % (
                item['ebayid'], item['price'], ebay_stock)
            open(self.ebaycsv, "a").write(line)

            category = self._get_category(item)

            if item['shipping_weight']:
                big_stock = item['stock']
            else:
                big_stock = 0

            line = "%s,%s,%s,%s,%s,%s\n" % (
                item['bigid'], item['name'], category, item['price'],
                item['shipping_weight'], big_stock)
            open(self.bigcsv, "a").write(line.encode("utf-8"))

            try:
                if stock >= 1 and item['shipping_weight']:
                    is_in_stock = 1
                    mage_stock = item['stock']
                else:
                    is_in_stock = 0
                    mage_stock = 0
            except:
                is_in_stock = 0
                mage_stock = 0

            line = "%s,%s,%s,%s,%s\n" % (
                item['asin'], mage_stock, item['price'],
                item['price'], is_in_stock)
            open(self.magecsv, "a").write(line.encode("utf-8"))

        print "csv making end"

    def spider_error(self, response, spider):
        # mark proxy not in use
        if not self.db:
            self._connect_db(spider)

        item = response.meta['item']

        sql = "UPDATE amazon2 SET stage3_amzn_error = '1' WHERE asin='%s'" % (item['asin'].strip())
        self.db.query(sql)
        self.db.commit()

    def process_item(self, item, spider):
        if not self.db:
            self._connect_db(spider)

        # if can_import:
        # in case item has price 0, set set stock 0
        if float(item['price']) < 1:
            item['stock'] = 0

        insert_sql = " deliver_australia='%s', price = '%s', stock = '%s', stage3_mage = 1, stage3_scrape_date = '%s' " % \
                     (item['deliver_australia'], item['price'], item['stock'], item['last_updated'])

        if spider.crawler.settings.get('ERROR_ONLY'):
            insert_sql = '%s, stage3_amzn_error=0 ' % insert_sql

        insert_sql = "UPDATE amazon2 SET %s WHERE asin = '%s'" % (insert_sql, item['asin'].strip())

        self.db.query(insert_sql)
        self.db.commit()

        return item

if __name__ == "__main__":
    obj = Amzn2Pipeline()
    obj.spider_closed()
