import scrapy
import re
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy.exceptions import CloseSpider
from amzn3.spiders.data import Data
from amzn3.captcha import Captcha
from amzn3.proxy import get_proxy
import MySQLdb
import MySQLdb.cursors
from scrapy import log
from time import gmtime, strftime
from amzn3.items import Amzn2Item


class AmazonSpider(scrapy.Spider):
    name = "amazon"
    amazon_uk = "https://www.amazon.co.uk/"
    amazon_base = "https://www.amazon.co.uk/dp/"

    date_check = ''
    sample_product = "B002APJCNE"

    allowed_domains = ["amazon.co.uk"]
    start_urls = (
        'https://www.amazon.co.uk',
    )
    captcha_count = 0
    headers = {
        'Accept-Encoding': 'gzip, deflate, sdch',
        'Accept-Language': 'en-US,en;q=0.8',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) '
                      'AppleWebKit/537.36 (KHTML, like Gecko)Chrome/47.0.2526'
                      '.73 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image'
                  '/webp,*/*;q=0.8',
        'Connection': 'keep-alive',
        'X-FirePHP-Version': '0.0.6'}
    referer = ''

    def __init__(self):
        log_file = '%s.log' % strftime("log/%Y%m%d%H%M", gmtime())
        logfile = open(log_file, 'a')
        log_observer = log.ScrapyFileLogObserver(logfile, level=log.DEBUG)
        log_observer.start()

    def _get_source(self):
        db_setting = self.crawler.settings.get("DB_SETTING")
        export_debug = self.crawler.settings.get('EXPORT_DEBUG')

        db = MySQLdb.connect(host=db_setting['host'],
                             user=db_setting['user'],
                             passwd=db_setting['passwd'],
                             db=db_setting['db'],
                             charset=db_setting['charset'],
                             cursorclass=MySQLdb.cursors.DictCursor)

        # proxy = self.crawler.settings.get("PROXY")
        # proxy_obj = get_proxy(db, proxy)

        sql = "SELECT asin, name, ebayid, bigid, shipping_weight, price FROM" \
              " amazon2"   # AND deliver_australia = 1"

        # if proxy_obj:
        #     range = ''
        #     if proxy_obj['stage3_from']:
        #         sql = '%s AND id >= %s' % (sql, proxy_obj['stage3_from'])
        #         range = '%s' % proxy_obj['stage3_from']
        #     else:
        #         range = 'MIN'
        #     if proxy_obj['stage3_to']:
        #         sql = '%s AND id <= %s' % (sql, proxy_obj['stage3_to'])
        #         range = '%s-%s' % (range, proxy_obj['stage3_to'])
        #     else:
        #         range = '%s-%s' % (range, 'MAX')
        # else:
        # range = 'MIN-MAX'

        if self.crawler.settings.get('ERROR_ONLY'):
            sql = '%s AND stage3_amzn_error = 1' % sql
            # range = '%s, error only' % range

        if export_debug:
            sql = '%s limit %s' % (sql, export_debug)

        log.msg('Scraping on range', level=log.INFO)
        # log.msg("sql here %s" % sql, level=log.INFO)

        cursor = db.cursor()
        cursor.execute(sql)

        return cursor

    def errorback(self, response):
        print response

    def parse(self, response):
        url = self.amazon_uk

        rr = Request(url=url, callback=self.search, headers=self.headers)
        return rr

    def search(self, response):
        url = "%s/s/ref=nb_sb_noss?url=search-alias%saps&field-keywords=%s" % (
            self.amazon_uk, '%3D', self.sample_product)

        # http://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=B002APJCN

        self.headers['Referer'] = response.url
        rr = Request(
            url=url, callback=self.search_landing, headers=self.headers)
        return rr

    def search_landing(self, response):
        # open("sample.html", "wb").write(response.body)

        captcha = Captcha(
            Selector(response), response.body,
            response.url, 'sample_cpatcha.html')

        if 'Robot Check' in response.body:
            # if self.captcha_count >= 20:
            #    raise CloseSpider('captcha_pass_failed')

            captcha = Captcha(
                Selector(response), response.body,
                response.url, 'sample_cpatcha.html')

            captcha_form = captcha.get_string()

            print ("Catcha --------- %s ------------" % captcha_form)

            rr = FormRequest.from_response(
                response,
                formdata=captcha_form,
                callback=self.search_landing
            )
            # rr.meta['item'] = response.request.meta['item']

            # self.captcha_count = self.captcha_count + 1

            return rr
        else:
            self.headers['Referer'] = response.url
            sel = Selector(response)
            url = sel.xpath(
                "//h2[contains(text(), 'Gro-Clock Sleep Trainer')]/../@href"
                ).extract()[0]

            rr = Request(
                url=url, callback=self.sample_delivery_date,
                headers=self.headers)
            return rr

    # def sample_page(self, response):
    #
    #     self.referer = response.url
    #
    #     url = 'http://www.amazon.co.uk/gp/product/features/dynamic-delivery-message/' \
    #           'udp-ajax-handler/get-delivery-message.html?_encoding=UTF-8'
    #
    #     self.form_data = {
    #         'asin': 'B002APJCNE',
    #         'deviceType': 'web',
    #         'preselectedMerchantId': '',
    #         'merchantId': 'A3P5ROKL5A1OLE',
    #         'updateSembu': '1',
    #         'countryCode': 'AU',
    #         'useDefaultShippingAddress': '0'}
    #
    #     headers = {
    #         'Origin': 'http://www.amazon.co.uk',
    #         'Accept-Encoding': 'gzip, deflate',
    #         'Accept-Language': 'en-US,en;q=0.8',
    #         'User-Agent': self.headers['User-Agent'],
    #         'Content-Type': 'application/x-www-form-urlencoded',
    #         'Accept': 'application/json, text/javascript, */*; q=0.01',
    #         'Referer': self.referer,
    #         'X-Requested-With': 'XMLHttpRequest',
    #         'Connection': 'keep-alive',
    #         'X-FirePHP-Version': self.headers['X-FirePHP-Version']}
    #
    #     rr = FormRequest(
    #         url, formdata=self.form_data, callback=self.sample_delivery_date,
    #         headers=headers, errback=self.errorback)
    #
    #     return rr

    def sample_delivery_date(self, response):
        # open("sample_delivery.html", "wb").write(response.body)
        #
        # pt = "Want it delivered to .*? by(.*?)\d+"
        # data = re.search(pt, response.body)
        #
        # try:
        #     date = re.sub("<.*?>", "", data.group(0)).split(",")[1].strip()
        # except:
        #     date = None
        #
        # if not date:
        #     raise CloseSpider('Delivery date for sample product'
        #                       '%s not found' % self.sample_product)
        #
        # self.date_check = date

        cursor = self._get_source()
        while 1:
            item = cursor.fetchone()
            if not item:
                break

            content = item['asin'].replace("\r", "").replace("\n", "")

            url = "%s%s" % (self.amazon_base, content)

            if self.referer:
                self.headers['Referrer'] = self.referer
                rr = Request(
                    url=url, callback=self.parse_item, headers=self.headers,
                    errback=self.asin_not_found,
                    dont_filter=True)
            else:
                rr = Request(
                    url=url, callback=self.parse_item,
                    errback=self.asin_not_found,
                    dont_filter=True)

            rr.meta['item'] = item
            self.referer = url
            yield(rr)

    def asin_not_found(self, response):
        self.item = Amzn2Item()
        meta = response.request.meta['item']
        self.item['asin'] = meta['asin']
        self.item['ebayid'] = meta['ebayid']
        self.item['bigid'] = meta['bigid']
        self.item['name'] = meta['name']
        self.item['shipping_weight'] = meta['shipping_weight']
        self.item['last_updated'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        self.item['price'] = 0
        self.item['stock'] = 0
        self.item['deliver_australia'] = 0
        self.item['raw_deliver'] = ''

        return self.item

    def parse_item(self, response):
        '''Parse the data on response return, in case 200'''
        sel = Selector(response)

        open('test.html', 'w').write(response.body)

        self.date_check = 'Want it delivered'

        if 'Robot Check' in response.body:
            # if self.captcha_count >= 20:
            #    raise CloseSpider('captcha_pass_failed')

            captcha = Captcha(Selector(response), response.body, response.url)

            captcha_form = captcha.get_string()

            rr = FormRequest.from_response(
                response,
                formdata=captcha_form,
                callback=self.parse_item
            )
            rr.meta['item'] = response.request.meta['item']

            self.captcha_count = self.captcha_count + 1

            return rr
        else:
            try:
                data = Data(
                    response.body,
                    sel,
                    response.request.meta['item'],
                    self.date_check
                )
                return data.item
            except:
                # case when item not found
                return self.asin_not_found(response)
