import scrapy
import re
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy.exceptions import CloseSpider
from amzn3.spiders.data import Data
from amzn3.captcha import Captcha
from amzn3.proxy import get_proxy
import MySQLdb
import MySQLdb.cursors
from scrapy import log
from time import gmtime, strftime


class AmazonSpider(scrapy.Spider):
    name = "amazon_countrysdfds"
    amazon_m = "http://www.amazon.co.uk/"
    amazon_base = "http://www.amazon.co.uk/dp/"
    stock_url = "http://www.amazon.co.uk/gp/product/features/dynamic-delivery-message/udp-ajax-handler/get-delivery-message.html?_encoding=UTF-8"
    date_check = ''

    allowed_domains = ["amazon.co.uk"]
    start_urls = (
        'https://www.amazon.co.uk',
    )

    captcha_count = 0

    headers = {'Content-Type': 'application/x-www-form-urlencoded',
               'X-Requested-With': 'XMLHttpRequest',
               'Connection': 'keep-alive',
               'Cache-Control': 'max-age=0',
               'X-Momentum-API-KEY': 'mBLAdWzDArSAcaAqgHsabA==',
               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}
    headers = {}   # no header

    def __init__(self):
        log_file = '%s.log' % strftime("log/%Y%m%d%H%M", gmtime())
        logfile = open(log_file, 'a')
        log_observer = log.ScrapyFileLogObserver(logfile, level=log.DEBUG)
        log_observer.start()

    def _get_source(self):
        db_setting = self.crawler.settings.get("DB_SETTING")
        db = MySQLdb.connect(host=db_setting['host'],
                             user=db_setting['user'],
                             passwd=db_setting['passwd'],
                             db=db_setting['db'],
                             charset=db_setting['charset'],
                             cursorclass=MySQLdb.cursors.DictCursor)

        proxy = self.crawler.settings.get("PROXY")
        proxy_obj = get_proxy(db, proxy)

        sql = "SELECT asin, name, ebayid, bigid, shipping_weight, price FROM amazon2 WHERE stage3_amzn = 1 AND deliver_australia = 1"

        if proxy_obj:
            range = ''
            if proxy_obj['stage3_from']:
                sql = '%s AND id >= %s' % (sql, proxy_obj['stage3_from'])
                range = '%s' % proxy_obj['stage3_from']
            else:
                range = 'MIN'
            if proxy_obj['stage3_to']:
                sql = '%s AND id <= %s' % (sql, proxy_obj['stage3_to'])
                range = '%s-%s' % (range, proxy_obj['stage3_to'])
            else:
                range = '%s-%s' % (range, 'MAX')
        else:
            range = 'MIN-MAX'

        if self.crawler.settings.get('ERROR_ONLY'):
            sql = '%s AND stage3_amzn_error = 1' % sql
            range = '%s, error only' % range

        log.msg('Scraping on range %s' % range, level=log.INFO)

        cursor = db.cursor()
        cursor.execute(sql)

        return cursor

    def parse(self, response):
        # url = "%s%s" % (self.amazon_base, 'B002APJCNE')

        data = {
            'deviceType': 'web',
            'preselectedMerchantId': '',
            'merchantId': 'A3P5ROKL5A1OLE',
            'updateSembu': '1',
            'countryCode': 'AU',
            'useDefaultShippingAddress': '0',
            'asin': 'B002APJCNE'
        }
        rr = FormRequest(
            self.stock_url,
            formdata=data,
            callback=self.test_result
        )

        return rr

    def test_result(self, response):
        open("B002APJCNE.html", "wb").write(response.body)

        pt = "Want it delivered to .*? by(.*?)\d+"
        data = re.search(pt, response.body)

        try:
            date = re.sub("<.*?>", "", data.group(0)).split(",")[1].strip()
        except:
            date = None

        if not date:
            raise CloseSpider('Delivery date for sample product B002APJCNE not found')

        self.date_check = date

        # gets the stock INFO

        cursor = self._get_source()
        while 1:
            item = cursor.fetchone()
            if not item:
                break

            asin = item['asin'].replace("\r", "").replace("\n", "")
            url = "%s%s/" % (self.amazon_base, asin)
            rr = Request(url=url, callback=self.parse_item)
            rr.meta['item'] = item

            yield(rr)

    def parse_item(self, response):
        '''Parse the data on response return, in case 200'''
        sel = Selector(response)

        if 'Robot Check' in response.body:
            # if self.captcha_count >= 20:
            #    raise CloseSpider('captcha_pass_failed')

            captcha = Captcha(Selector(response), response.body, response.url)
            captcha_form = captcha.get_string()
            rr = FormRequest.from_response(
                response,
                formdata=captcha_form,
                callback=self.parse_item
            )
            rr.meta['item'] = response.request.meta['item']
            self.captcha_count = self.captcha_count + 1

            return rr
        else:
            data = Data(
                response.body,
                sel,
                response.request.meta['item'],
                self.date_check
            )
            return data.item
