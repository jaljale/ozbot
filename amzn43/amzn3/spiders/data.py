'''For data cleaning from html page'''
import re
import urllib
import json
from time import gmtime, strftime
from amzn3.items import Amzn2Item
from amzn3 import str
from scrapy.selector import Selector
from BeautifulSoup import BeautifulSoup as Soup
from scrapy import log


class Data():
    '''cleaned and return data item'''
    def __init__(self, html, sel, item, date_check):

        self.html = html
        self.sel = sel
        self.item_data = item
        self.date_check = date_check

        self.item = Amzn2Item()
        self.item['asin'] = item['asin']
        self.item['ebayid'] = item['ebayid']
        self.item['bigid'] = item['bigid']
        self.item['name'] = item['name']
        self.item['shipping_weight'] = item['shipping_weight']

        self.item['last_updated'] = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        self._can_deliver()

        try:
            self._get_stock()
            self._get_price()
        except Exception as e:
            log.msg("Item not found (%s)" % self.item['asin'])
            log.msg("%s" % e)
            # item not found cases
            self.item['price'] = 0
            self.item['stock'] = 0

        self._calculate_price()

    def encode(self, string):
        str_val = str.encode(string)
        return self._get_striped(str_val)

    def _get_striped(self, string):
        return str.get_striped(string)

    def _get_float(self, string):
        '''remove the string and return floatingpoint'''
        float = re.search("(\d+)+\.?(\d+)*", string)
        return float.group(0)

    def _calculate_price(self):
        # try:

        if not self.item['price']:
            self.item['stock'] = 0
        else:
            if self.item['price'] and self.item_data['shipping_weight']:
                # sometimes price will come with decimal in two place

                pcs = self.item['price'].split('.')
                if (len(pcs) > 2):
                    price = '%s.%s' % (pcs[0], pcs[1])
                else:
                    price = self.item['price']

                pcs = self.item_data['shipping_weight'].split('.')
                if (len(pcs) > 2):
                    shipping_weight = '%s.%s' % (pcs[0], pcs[1])
                else:
                    shipping_weight = self.item['shipping_weight']

                # print self.item, price, shipping_weight

                price = (float(price) / 1.2) + (float(shipping_weight) * 8)
                price = price * 2.15
                price = price + 16
                price = price / 0.82
                price = price * 1.1
                price = round(price, 0)
                price = price - 0.01
                self.item['price'] = price

    def _get_price(self):
        price = 0
        try:
            price = self.sel.xpath('//td[contains(text(),"Price:")]/following-sibling::td/span/text()').extract()[0]
        except:
            try:
                price = self.sel.xpath("//span[@class='olp-padding-right']/span[@class='a-color-price']/text()").extract()[0]
            except:
                price = self.sel.xpath("//span[@id='priceblock_ourprice']/text()").extract()[0]

        try:
            price = re.findall('\d+[.\d+]+', price)[0].strip()
        except:
            price = 0

        self.item['price'] = price

    def _can_deliver(self):
        stock = 0
        self.item['deliver_australia'] = 0

        pt = "Want it delivered to"
        data = re.search(pt, self.html)

        if data:
            self.item['deliver_australia'] = 1
        else:
            self.item['deliver_australia'] = 0

    def _get_stock(self):
        stock = 0
        self.item['deliver_australia'] = 0

        pt = "Want it delivered"
        data = re.search(pt, self.html)

        if data:
            stock = 10
        else:
            stock = 0

        # self.item['raw_deliver'] = None
        # pt = "Want it delivered to .*? by(.*?)\d+"
        # data = re.search(pt, self.html)
        #
        # if data:
        #     # try:
        #     date = data.group(0)
        #     self.item['raw_deliver'] = date
        #     if self.date_check in date:
        #         stock = 10
        #
        #     # except:
        #     #    stock = 0
        # else:
        #     stock = 0

        if stock:
            pt2 = re.compile("Only (\d+) left in stock", re.I)
            found2 = re.findall(pt2, self.html)

            if found2:
                stock = found2[0]

        self.item['stock'] = stock

    def get_data(self):
        return self.item
