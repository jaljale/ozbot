# Scrapy settings for amzn2 project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
import os
import sys

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

BOT_NAME = 'amzn3'

SPIDER_MODULES = ['amzn3.spiders']
NEWSPIDER_MODULE = 'amzn3.spiders'

ITEM_PIPELINES = ['amzn3.pipelines.Amzn2Pipeline']

# the user-agent
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'
# iPhone
# USER_AGENT = 'Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3'

COOKIES_ENABLED = True
# COOKIES_DEBUG = True
AUTOTHROTTLE_ENABLED = True
AUTOTHROTTLE_START_DELAY = 7


DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
    'amzn3.middlewares.ProxyMiddleware': 100,
}

ITEM_PIPELINES = {
    'amzn3.pipelines.Amzn2Pipeline': 300,
}

PROXY = 0
DB_SETTING = {'host': 'localhost',
              'user': 'root',
              'passwd': '',
              'db': 'amazon',
              'charset': 'utf8'}

PROXY_OBJ = ''
ERROR_ONLY = False


print BASE_DIR

EXPORT_PATH = os.path.join(BASE_DIR, 'export')

EXPORT_DEBUG = False
