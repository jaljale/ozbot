# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class Amzn2Item(Item):
    asin = Field()
    name = Field()
    shipping_weight = Field()
    deliver_australia = Field()
    ebayid = Field()
    bigid = Field()
    raw_deliver = Field()
    price = Field()
    stock = Field()
    last_updated = Field()
