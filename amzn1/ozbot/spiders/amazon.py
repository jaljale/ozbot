import scrapy
import os
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy.exceptions import CloseSpider
from ozbot.spiders.data import Data
from ozbot.captcha import Captcha
from ozbot.proxy import get_proxy
from scrapy import log
from time import gmtime, strftime


class AmazonSpider(scrapy.Spider):
    name = "amazon"
    amazon_m = "http://www.amazon.co.uk/gp/product/"
    amazon_base = "http://www.amazon.co.uk/dp/"
    allowed_domains = ["amazon.co.uk"]
    start_urls = (
        'http://www.amazon.co.uk/',
    )

    captcha_count = 0

    proxy = 0

    def __init__(self, **kwargs):
        super(AmazonSpider, self).__init__(**kwargs)
        log_file = '%s.log' % strftime("log/%Y%m%d%H%M", gmtime())
        logfile = open(log_file, 'a')
        log_observer = log.ScrapyFileLogObserver(logfile, level=log.DEBUG)
        log_observer.start()

    def _get_csv(self):
        file = self._get_source()

        if not file or not file['source']:
            source = 'stage1.csv'
        else:
            source = file['source']

        file_path = "%s/../source/%s" % (os.path.dirname(os.path.realpath(__file__)), source)

        if not os.path.isfile(file_path):
            raise CloseSpider("Source %s not found" % source)
        else:
            log.msg("Using source %s" % source, level=log.INFO)
            return file_path

    def _get_source(self):
        proxy = self.crawler.settings.get("PROXY")
        db = self.crawler.settings.get("DB_SETTING")

        self.proxy = get_proxy(db, proxy)

        return self.proxy

    def parse(self, response):
        fh = open(self._get_csv(), "r")

        referrer = ''
        while 1:
            content = fh.readline()
            if not content: break

            content = content.replace("\r", "").replace("\n", "")
            url = self.amazon_base + content

            if referrer:
                rr = Request(url = url, callback = self.parse_item, headers = {'Referer':referrer})
            else:
                rr = Request(url = url, callback = self.parse_item)

            rr.meta['asin'] = content
            referrer = url
            yield(rr)

    def parse_item(self, response):
        '''Parse the data on response return, in case 200'''
        sel = Selector(response)

        if '404 - Document Not Found' in response.body:
            return None

        if 'Robot Check' in response.body:
            #if self.captcha_count >= 20:
            #    raise CloseSpider('captcha_pass_failed')

            captcha = Captcha(Selector(response), response.body, response.url)

            captcha_form = captcha.get_string()

            rr = FormRequest.from_response(response,
                    formdata=captcha_form,
                    callback=self.parse_item)
            rr.meta['asin'] = response.request.meta['asin']

            self.captcha_count = self.captcha_count + 1

            return rr

            #self._death_captcha(response)

        else:
            data = Data(response.body, sel, response.request.meta['asin'], self.proxy)
            data.item['asin'] = response.request.meta['asin']
            return data.item
